import logging.config

import cv2
import matplotlib.pyplot as plt
import numpy as np

BENCHMARK_LOGGER_CONFIG_FILE = "benchmark_logger.conf"
STARTED = "started"
FINISHED = "finished"
ELAPSED = "elapsed"
RESULT = "result"
ALGORITHMS_KEYPOINTS_LIST = ["sift", "surf", "orb", "fast", "star"]
ALGORITHMS_DESCRIPTORS_LIST = ["sift", "surf", "orb", "brief"]
ALGORITHMS_MATCHERS_LIST = ["bf", "flann"]


class Benchmark:
    def __init__(self, kp_algs, desc_algs, matcher_algs):
        logging.config.fileConfig(BENCHMARK_LOGGER_CONFIG_FILE)
        self.logger = logging.getLogger("benchmark")
        self.results = {}

        if not kp_algs == "all":
            keypoints_algorithms = [algorithm for algorithm in kp_algs if algorithm in ALGORITHMS_KEYPOINTS_LIST]
        else:
            keypoints_algorithms = ALGORITHMS_KEYPOINTS_LIST

        if not desc_algs == "all":
            descriptors_algorithms = [algorithm for algorithm in desc_algs if algorithm in ALGORITHMS_DESCRIPTORS_LIST]
        else:
            descriptors_algorithms = ALGORITHMS_DESCRIPTORS_LIST

        if not matcher_algs == "all":
            matchers_algorithms = [algorithm for algorithm in matcher_algs if algorithm in ALGORITHMS_MATCHERS_LIST]
        else:
            matchers_algorithms = ALGORITHMS_MATCHERS_LIST

        self.logger.info("Benchmark started.")
        self.start({
            "keypoints": keypoints_algorithms,
            "descriptors": descriptors_algorithms,
            "matcher": matchers_algorithms
        })
        self.logger.info("Benchmark ended.")

    def start(self, algorithms):
        benchmark_part = self.benchmark
        for kp_alg in algorithms["keypoints"]:
            for desc_alg in algorithms["descriptors"]:
                for matcher_alg in algorithms["matcher"]:
                        benchmark_part(kp_alg, desc_alg, matcher_alg)

    def benchmark(self, kp, desc, matcher):
        key = "".join([kp, "/", desc, "/", matcher])
        self._startTimer(key)
        self.results[key][RESULT] = self.evaluate(kp, desc, matcher)
        self._endTimer(key)

    @staticmethod
    def evaluate(kp, desc, matcher):
        from benchmark.Algorithm import Algorithm
        algo = Algorithm(kp, desc, matcher)
        return algo.results

    def compute_results_average(self):
        averages = []

        for key in self.results.keys():
            total_key = 0

            for rank in self.results[key][RESULT].keys():
                total_rank = 0

                for sample in self.results[key][RESULT][rank]:
                    total_rank += self.results[key][RESULT][rank][sample]

                total_rank /= np.float32(len(self.results[key][RESULT][rank]))
                total_key += total_rank

            total_key /= np.float32(len(self.results[key][RESULT]))
            averages.append(total_key)

        return np.around(averages, decimals=2)

    def plot(self):
        times = [self.results[key][ELAPSED] for key in self.results.keys()]
        keys = list(self.results.keys())
        indexes = range(len(keys))
        results_average = self.compute_results_average()

        plt.title('Benchmark')
        plt.xlabel('Algorithms')
        plt.ylabel('Time (in seconds)')
        plt.xticks(indexes, keys, rotation=45)
        plt.grid(True)

        plt.plot(indexes, times)

        for i, average in enumerate(results_average):
            plt.annotate(average, (indexes[i], times[i]))

        plt.show()

    def _startTimer(self, key):
        self.results[key] = {STARTED: cv2.getTickCount(), FINISHED: None, ELAPSED: None}

    def _endTimer(self, key):
        if key not in self.results:
            self.logger.warning("The " + key + " timer does not exists. Please use _startTimer.")
        else:
            self.results[key][FINISHED] = cv2.getTickCount()
            self.results[key][ELAPSED] = \
                (self.results[key][FINISHED] - self.results[key][STARTED]) / cv2.getTickFrequency()
            self.logger.info("The " + key + " timer has ended: " + str(self.results[key][ELAPSED]) + " seconds.")

if __name__ == '__main__':
    # star returns only 0
    # orb descriptor is broken?
    benchmark = Benchmark(["sift", "surf", "orb", "fast"], ["sift", "surf", "brief"], "all")
    benchmark.plot()
