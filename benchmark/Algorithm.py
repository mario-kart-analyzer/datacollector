import logging
import os

import cv2
import numpy as np

from benchmark.Benchmark import ALGORITHMS_MATCHERS_LIST, ALGORITHMS_KEYPOINTS_LIST, ALGORITHMS_DESCRIPTORS_LIST
from datacollector.definitions import BENCHMARK_DIR

IMAGES_PATH = os.path.join(BENCHMARK_DIR, "images")
TEMPLATES_PATH = os.path.join(BENCHMARK_DIR, "templates")


class Algorithm:
    def __init__(self, keypoints_algorithm, descriptors_algorithm, matcher_algorithm):
        self.keypoints_algorithm = keypoints_algorithm
        self.descriptors_algorithm = descriptors_algorithm
        self.matcher_algorithm = matcher_algorithm
        self.results = {}
        self.logger = logging.getLogger("benchmark")

        if self.keypoints_algorithm not in ALGORITHMS_KEYPOINTS_LIST:
            self.logger.error("The keypoint extraction algorithm is not available. Aborting.")
        elif self.descriptors_algorithm not in ALGORITHMS_DESCRIPTORS_LIST:
            self.logger.error("The keypoint descriptor algorithm is not available. Aborting.")
        elif self.matcher_algorithm not in ALGORITHMS_MATCHERS_LIST:
            self.logger.error("The matcher algorithm is not available. Aborting.")
        else:
            self.start()

    def start(self):
        for rank in range(1, 13):
            rank_img = cv2.imread(os.path.join(TEMPLATES_PATH, str(rank)) + ".png", cv2.IMREAD_GRAYSCALE)
            self.results[rank] = {}
            for i in range(1, 11):
                self.results[rank][i] = len(self.evaluate(rank, rank_img, i))

    def evaluate(self, rank, rank_img, i):
        kp_rank = None
        kp_i = None
        i_img = cv2.imread(os.path.join(IMAGES_PATH, str(rank), str(i)) + ".png", cv2.IMREAD_GRAYSCALE)

        if self.keypoints_algorithm == self.descriptors_algorithm:
            if self.keypoints_algorithm == "sift":
                return self.evaluate_sift_both(rank_img, i_img)
            elif self.keypoints_algorithm == "surf":
                return self.evaluate_surf_both(rank_img, i_img)
            elif self.keypoints_algorithm == "orb":
                return self.evaluate_orb_both(rank_img, i_img)
        else:
            if self.keypoints_algorithm == "sift":
                kp_rank, kp_i = self.evaluate_sift_kp(rank_img, i_img)
            elif self.keypoints_algorithm == "surf":
                kp_rank, kp_i = self.evaluate_surf_kp(rank_img, i_img)
            elif self.keypoints_algorithm == "orb":
                kp_rank, kp_i = self.evaluate_orb_kp(rank_img, i_img)
            elif self.keypoints_algorithm == "fast":
                kp_rank, kp_i = self.evaluate_fast_kp(rank_img, i_img)
            elif self.keypoints_algorithm == "star":
                kp_rank, kp_i = self.evaluate_star_kp(rank_img, i_img)

            if self.descriptors_algorithm == "sift":
                return self.evaluate_sift_des(rank_img, i_img, kp_rank, kp_i)
            elif self.descriptors_algorithm == "surf":
                return self.evaluate_surf_des(rank_img, i_img, kp_rank, kp_i)
            elif self.descriptors_algorithm == "orb":
                return self.evaluate_orb_des(rank_img, i_img, kp_rank, kp_i)
            elif self.descriptors_algorithm == "brief":
                return self.evaluate_brief_des(rank_img, i_img, kp_rank, kp_i)

    """
    Keypoints extractors
    """

    @staticmethod
    def evaluate_sift_kp(rank, i):
        sift = cv2.xfeatures2d.SIFT_create()
        kp_rank = sift.detect(rank, None)
        kp_i = sift.detect(i, None)
        return kp_rank, kp_i

    @staticmethod
    def evaluate_surf_kp(rank, i):
        surf = cv2.xfeatures2d.SURF_create(400)
        kp_rank = surf.detect(rank, None)
        kp_i = surf.detect(i, None)
        return kp_rank, kp_i

    @staticmethod
    def evaluate_orb_kp(rank, i):
        cv2.ocl.setUseOpenCL(False)  # orb is broken, workaround: see issue #6081 in opencv github repo
        orb = cv2.ORB_create()
        kp_rank = orb.detect(rank, None)
        kp_i = orb.detect(i, None)
        cv2.ocl.setUseOpenCL(True)
        return kp_rank, kp_i

    @staticmethod
    def evaluate_fast_kp(rank, i):
        fast = cv2.FastFeatureDetector_create(5, False)
        kp_rank = fast.detect(rank, None)
        kp_i = fast.detect(i, None)
        return kp_rank, kp_i

    @staticmethod
    def evaluate_star_kp(rank, i):
        star = cv2.xfeatures2d.StarDetector_create()
        kp_rank = star.detect(rank, None)
        kp_i = star.detect(i, None)
        return kp_rank, kp_i

    """
    Keypoints descriptors
    """

    def evaluate_sift_des(self, rank, i, kp_rank, kp_i):
        sift = cv2.xfeatures2d.SIFT_create()
        kp_rank, des_rank = sift.compute(rank, kp_rank)
        kp_i, des_i = sift.compute(i, kp_i)
        return self.match(des_rank, des_i)

    def evaluate_surf_des(self, rank, i, kp_rank, kp_i):
        surf = cv2.xfeatures2d.SURF_create(400)
        kp_rank, des_rank = surf.compute(rank, kp_rank)
        kp_i, des_i = surf.compute(i, kp_i)
        return self.match(des_rank, des_i)

    def evaluate_orb_des(self, rank, i, kp_rank, kp_i):
        cv2.ocl.setUseOpenCL(False)  # orb is broken, workaround: see issue #6081 in opencv github repo
        orb = cv2.ORB_create()
        kp_rank, des_rank = orb.compute(rank, kp_rank)
        kp_i, des_i = orb.compute(i, kp_i)
        return self.match(des_rank, des_i)

    def evaluate_brief_des(self, rank, i, kp_rank, kp_i):
        brief = cv2.xfeatures2d.BriefDescriptorExtractor_create()
        kp_rank, des_rank = brief.compute(rank, kp_rank)
        kp_i, des_i = brief.compute(i, kp_i)
        return self.match(des_rank, des_i)

    """
    Both
    """

    def evaluate_sift_both(self, rank, i):
        sift = cv2.xfeatures2d.SIFT_create()
        kp_rank, des_rank = sift.detectAndCompute(rank, None)
        kp_i, des_i = sift.detectAndCompute(i, None)
        return self.match(des_rank, des_i)

    def evaluate_surf_both(self, rank, i):
        surf = cv2.xfeatures2d.SURF_create(400)
        kp_rank, des_rank = surf.detectAndCompute(rank, None)
        kp_i, des_i = surf.detectAndCompute(i, None)
        return self.match(des_rank, des_i)

    def evaluate_orb_both(self, rank, i):
        cv2.ocl.setUseOpenCL(False)  # orb is broken, workaround: see issue #6081 in opencv github repo
        orb = cv2.ORB_create()
        kp_rank = orb.detect(rank, None)
        kp_i = orb.detect(i, None)
        kp_rank, des_rank = orb.compute(rank, kp_rank)
        kp_i, des_i = orb.compute(i, kp_i)
        return self.match(des_rank, des_i)

    """
    Matchers
    """

    def match(self, des1, des2):
        if self.matcher_algorithm == "bf":
            return self.bf_match(des1, des2)
        elif self.matcher_algorithm == "flann":
            return self.flann_match(des1, des2)

    def bf_match(self, des1, des2):
        if self.descriptors_algorithm == "orb" or self.descriptors_algorithm == "brief":
            bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
        else:
            bf = cv2.BFMatcher(cv2.NORM_L2, crossCheck=True)

        return bf.match(des1, des2)

    def flann_match(self, des1, des2):
        # convert descriptors to float32
        des1 = np.float32(des1)
        des2 = np.float32(des2)

        # flann parameters
        index_params = dict(algorithm=0, trees=5)
        search_params = dict(checks=50)
        flann = cv2.FlannBasedMatcher(index_params, search_params)

        matches = flann.knnMatch(des1, des2, k=2)

        # ratio test as per Lowe's paper
        good_matches = [matches[i] for i, (m, n) in enumerate(matches) if m.distance < 0.7 * n.distance]

        return good_matches
