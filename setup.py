from distutils.core import setup

setup(
    name='datacollector',
    version='0.2',
    packages=['datacollector',
              'datacollector.utils',
              'datacollector.states',
              'datacollector.cutting',
              'datacollector.indicators',
              'datacollector.indicators.menus_indicators',
              'datacollector.corrector',
              'datacollector.guis',
              'datacollector.learning',
              'datacollector.learning.builder'],
    url='',
    license='GNU GPL v3',
    author='Simon Bar, Sylvain Bossut, Pierre Coiffier',
    author_email='',
    description='Mario Kart data collector',
    requires=['numpy',
              'Pillow',
              'pyocr',
              'sklearn',
              'tensorflow',
              'prettytensor',
              'matplotlib']
)
