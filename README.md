# Mario Kart data collector 

This tool extracts informations and statistics from Mario Kart 8 (on Wii U).

## Dependencies
- Python 3.5
- opencv with opencv_contrib modules
- numpy
- sklearn
- tensorflow
- prettytensor
- matplotlib
- Pillow
- pyocr
- pytest (unit tests)

## Usage

No argument means recording directly from the video flux of the capture card.

````commandline
python -m datacollector.collector
````

`-v` argument means recording from an existing video (supported formats are those 
supported by OpenCV). Empty string by default.

`--debug_indicators` means displaying the windows showing both the video and the
requested indicators for the frame being. False by default.

## Utilities

### Retrieve a list of images from a video. 

The video formats supported are the same as the ones supported by OpenCV.

```commandline
python -m datacollector.utils.extract_video video1
```

You can set your custom destination folder

```commandline
python -m datacollector.utils.extract_video video1 --images_subfolder my-custom-image-folder
```

You can also change the image extraction period (in ms)

```commandline
python -m datacollector.utils.extract_video video1 --images_subfolder my-custom-image-folder --period 5000
```

### Build your own image classifier

Call from code

```python
import tensorflow as tf

from datacollector.learning.builder.ImageClassifierBuilder import ImageClassifierBuilder

with tf.variable_scope("classifier_name"):
    builder = ImageClassifierBuilder(name="classifier_name", images_path="path/to/images")
    builder.train()
```

Or call the script itself
```commandline
python -m datacollector.learning.builder.build classifier_name "path/to/images"
```

## Project structure

```
datacollector/
├── benchmark/                  * OpenCV feature detection and description benchmark
├── datacollector/              
│   ├── corrector/              * All indicator correctors
│   ├── cutting/                * All cutting services (eg crop one player screen part)
│   ├── guis/                   * Our guis (output GUI, game GUI and correctors GUI)
│   ├── indicators/             * All indicators declarations
│   ├── learning/               * Machine learning stuff
│   ├── states/                 * All state declarations
│   └── utils/                  * Some useful functions
│   
└── templates/                  * All templates stored by indicators
```