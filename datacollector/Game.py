import json
import logging
import os
import time
from datetime import datetime

from datacollector.GameSummary import GameSummary
from datacollector.definitions import GAME_JSON_FILE, RACES_DIR
from datacollector.guis.GameGUI import GameGUI

GameGUI().start()
mk_logger = logging.getLogger("mario_kart")
summary = GameSummary()


class Player(object):
    """Represent a player"""

    def __init__(self, player_id):
        """Instantiate a player
        
        :param player_id: What identifies the player
        :type player_id: int
        """

        super().__init__()
        self.player_id = player_id
        self.race_position = 0
        self.current_lap = 0
        self.current_item = ""
        self.score = 0
        self.finished_race = True
        self.race_times = {
            "start": 0,
            "total": 0,
            "pause_total": 0,
            "pause_start": 0,
            "delta": 0,
            "laps": []
        }
        self.items_count = {}

    def format_position(self):
        """Format the position for GameGUI"""

        suffixes = ["", "st", "nd", "rd"] + ["th"] * 9
        if not self.race_position:
            return ""
        if self.race_position < 1:
            return ""
        else:
            return str(self.race_position) + suffixes[self.race_position % 100]

    def format_time(self, time_id):
        """Format time for GameGUI"""

        seconds = self.race_times[time_id]
        minutes = int(seconds / 60)
        seconds_left = seconds - (minutes * 60)
        return "%d:%.3f" % (minutes, seconds_left)

    def start_timer(self):
        """Start the timer for race time"""

        self.race_times["laps"] = []
        self.race_times["total"] = 0
        self.race_times["delta"] = 0
        self.race_times["pause_total"] = 0
        self.race_times["pause_start"] = 0
        self.race_times["start"] = time.time()

    def stop_timer(self, previous_time):
        """Stop the timer and save it; do nothing if it hasn't started"""

        if self.race_times["start"] == 0:
            mk_logger.warning("Can't stop the timer: it hasn't started.")
            return

        self.race_times["total"] = time.time() - self.race_times["start"] - self.race_times["pause_total"]
        self.race_times["start"] = 0
        self.race_times["delta"] = 0 if previous_time == 0 else self.race_times["total"] - previous_time

    def pause_timer(self):
        """Pause the timer; do nothing if it hasn't started"""

        if self.race_times["start"] == 0:
            mk_logger.warning("Can't pause the timer: it hasn't started.")
            return

        self.race_times["pause_start"] = time.time()

    def resume_timer(self):
        """Resume the timer; do nothing if it hasn't started or if it wasn't paused"""

        if self.race_times["start"] == 0:
            mk_logger.warning("Can't resume the timer: it hasn't started.")
            return
        elif self.race_times["pause_start"] == 0:
            mk_logger.warning("Can't resume the timer: it wasn't paused.")
            return

        self.race_times["pause_total"] += time.time() - self.race_times["pause_start"]
        self.race_times["pause_start"] = 0

    def save_lap_timer(self):
        """Save current time value without stopping it; do nothing if it hasn't started"""

        if self.race_times["start"] == 0:
            mk_logger.warning("Can't save lap timer: it hasn't started.")
            return

        laps_finished = len(self.race_times["laps"])
        last_lap_time = 0

        if laps_finished > 0:
            for i in range(0, laps_finished):
                last_lap_time += self.race_times["laps"][i]

        lap_time = time.time() - self.race_times["start"] - self.race_times["pause_total"] - last_lap_time

        if lap_time > 5:
            self.race_times["laps"].append(lap_time)
            mk_logger.info("Player %d new lap time: %.3f" % (self.player_id, self.race_times["laps"][-1]))

    def change_item(self, item):
        """Change current item and add it to the items received counter"""

        self.current_item = item

        if item not in self.items_count.keys():
            self.items_count[item] = 1
        else:
            self.items_count[item] += 1


class Game(object):
    """The game class"""

    def __init__(self, player_count=None):
        """Instantiate a game
        
        :param player_count: Number of players
        :type player_count: int
        """

        self.race_in_progress = False
        self.current_state = ""
        self.current_race = ""
        self.last_scoreboard = {k: {"type": "ai", "score": 0} for k in range(1, 13)}

        if player_count is None:
            self.players_count = 0
            self.players = {}
        else:
            self.init_players(player_count)

        self.update()

    def __setattr__(self, key, value):
        super().__setattr__(key, value)
        mk_logger.info("Game: the value " + str(key) + " changed to " + str(value))
        self.update()

    def init_players(self, player_count):
        """Instantiate a certain number of players and update
        
        :param player_count: Number of players
        :type player_count: int
        """

        self.players_count = player_count
        self.players = {}
        for i in range(1, self.players_count + 1):
            self.players[i] = Player(i)
        self.update()

    def start_timers(self):
        """Start timer for each player and update"""

        for i in range(1, self.players_count + 1):
            self.players[i].start_timer()
        self.update()

    def stop_timers(self):
        """Stop timer for each player and update"""

        for i in range(1, self.players_count + 1):
            self.players[i].stop_timer(0)
        self.update()

    def pause_timers(self):
        """Pause timer for each player and update"""

        for i in range(1, self.players_count + 1):
            self.players[i].pause_timer()
        self.update()

    def resume_timers(self):
        """Resume timer for each player and update"""

        for i in range(1, self.players_count + 1):
            self.players[i].resume_timer()
        self.update()

    def update(self):
        """Serialize current game status into a json file"""

        with open(GAME_JSON_FILE, 'w') as jsonfile:
            json_str = json.dumps(self, cls=Encoder, sort_keys=True, separators=(',', ': '))
            json_str = str(json_str).strip("'<>() ").replace('\'', '\"')  # avoid non-json conforming quoting
            json.dump(json_str, jsonfile)

    def save_state(self):
        """Serialize relevent informations of the current game status (usually of last race) into a json file"""

        self.update()
        summary.digest(self)

        file_name = "Mario Kart race " + datetime.now().strftime("%Y-%m-%d %H-%M-%S") + ".json"
        file_path = os.path.join(RACES_DIR, file_name)

        with open(file_path, 'w') as jsonfile:
            json_str = json.dumps(summary, cls=Encoder, sort_keys=True)
            json.dump(json_str, jsonfile)


class Encoder(json.JSONEncoder):
    def default(self, o):
        return o.__dict__
