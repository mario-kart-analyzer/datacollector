from datacollector.indicators.InGameIndicator import InGameIndicator
from datacollector.indicators.ItemIndicator import ItemIndicator
from datacollector.indicators.LapIndicator import LapIndicator
from datacollector.indicators.LoadingIndicator import LoadingIndicator
from datacollector.indicators.PositionIndicator import PositionIndicator
from datacollector.indicators.RaceBeginningIndicator import RaceBeginningIndicator
from datacollector.indicators.RaceDetectIndicator import RaceDetectIndicator
from datacollector.indicators.RaceEndingIndicator import RaceEndingIndicator
from datacollector.indicators.menus_indicators.WelcomeMenuIndicator import WelcomeMenuIndicator
from datacollector.indicators.menus_indicators.menu_indicator_list import menu_indicator_list

indicator_list = {
    LoadingIndicator.id: LoadingIndicator,
    InGameIndicator.id: InGameIndicator,
    WelcomeMenuIndicator.id: WelcomeMenuIndicator,
    RaceDetectIndicator.id: RaceDetectIndicator,
    RaceEndingIndicator.id: RaceEndingIndicator,
    LapIndicator.id: LapIndicator,
    RaceBeginningIndicator.id: RaceBeginningIndicator,
    PositionIndicator.id: PositionIndicator,
    ItemIndicator.id: ItemIndicator
}


indicator_list.update(menu_indicator_list)
