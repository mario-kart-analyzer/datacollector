import os

import cv2
from numpy.core.multiarray import ndarray

from datacollector.definitions import TEMPLATES_RACE_ENDING_DIR
from datacollector.indicators import Indicator


class RaceEndingIndicator(Indicator.Indicator):
    """Indicator class which detects the race ending, when the scoreboard is displayed"""

    id = "RACE_ENDING_INDICATOR"

    def __init__(self, params):
        super().__init__(params)

        assert 1 <= self.params["player_count"] <= 4

        self.players_count = self.params["player_count"]

        if self.players_count == 4 or self.players_count == 3:
            self.threshold = 3300000
            template = cv2.imread(os.path.join(TEMPLATES_RACE_ENDING_DIR, "end4.png"), cv2.IMREAD_COLOR)
        else:  # different templates if there is 2 or 4 players
            self.threshold = 7500000
            template = cv2.imread(os.path.join(TEMPLATES_RACE_ENDING_DIR, "end2.png"), cv2.IMREAD_COLOR)

        template = cv2.resize(template, (0, 0), fx=0.25, fy=0.25)
        self.template_gray = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)

    def get_information(self, image: ndarray) -> bool:
        """Detect whether the race is ending (according to the 'finished' message)
        
        :param image: In game image
        :type image: ndarray
        :return: True if the race is ending
        :rtype: bool
        """

        # resizing at 0.25 ratio
        image = cv2.resize(image, (0, 0), fx=0.25, fy=0.25)

        result = cv2.matchTemplate(image, self.template_gray, cv2.TM_CCOEFF)
        minval, maxval, minloc, maxloc = cv2.minMaxLoc(result)

        return maxval > self.threshold
