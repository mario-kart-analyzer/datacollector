import difflib
import logging

import cv2
import pyocr
from PIL import Image
from numpy.core.multiarray import ndarray

from datacollector import definitions
from datacollector.cutting.indicators.race_detect_cutter import crop_roi
from datacollector.indicators import Indicator


class RaceDetectIndicator(Indicator.Indicator):
    """Indicator which detects the race name with OCR"""

    id = "RACE_DETECT_INDICATOR"

    def __init__(self, params):
        """Instantiate the indicator; if no OCR tool found, the get_information method will not work
        
        :param params: Indicator parameters
        :type params: dict of (str, ?)
        """

        super().__init__(params)
        self.mk_logger = logging.getLogger("mario_kart")
        self.images = []

        tools = pyocr.get_available_tools()
        if len(tools) == 0:
            self.mk_logger.error("No OCR tool found!")
            self.ocr_tool = None
        else:
            self.ocr_tool = tools[0]

    def get_information(self, image: ndarray) -> str:
        """Read on an image the name of the current race.
        
        :param image: In game image
        :type image: ndarray
        :return: The race's name; or an empty string if what is read is not close enough of the available races, or if
        no OCR tool is found
        :rtype: str
        """

        if self.ocr_tool is None:
            self.mk_logger.error("No OCR tool found, detecting the race name is impossible.")
            return ""

        # crop region of interest and convert image to gray
        # detect race name
        # check close matches in the known list of races

        image = crop_roi(cv2.cvtColor(image, cv2.COLOR_BGR2GRAY))

        try:
            name_detected = self.ocr_tool.image_to_string(Image.fromarray(image))
        except UnicodeDecodeError:  # the detection can raise an exception if the text has non-unicode characters
            return ""

        close_matches = difflib.get_close_matches(name_detected, definitions.RACES_NAMES)

        if len(close_matches) == 1:
            return close_matches[0]
        elif name_detected in close_matches:
            return name_detected

        for close_match in close_matches:
            # if 'race' in '#race####' or if 'rac' in 'race'
            if close_match in name_detected or name_detected in close_match:
                return close_match

        return ""  # no match found

    def clear_images(self):
        self.images.clear()

    def append_image(self, image: ndarray):
        self.images.append(image)

    def detect_images(self):
        """Detect from the images the race's name
        
        :return: A corresponding match, or an empty string if none found
        :rtype: str
        """

        match = ""

        for image in self.images:
            if match == "":
                match = self.get_information(image)
            if not match == "":
                return match

        return match
