import os

import cv2
import numpy as np

from datacollector.cutting.in_game_stats import in_game_stats
from datacollector.definitions import LAP, TEMPLATES_LAPS_IN_GAME_DIR
from datacollector.indicators import Indicator

threshold = 2200


class LapIndicator(Indicator.Indicator):
    """Indicator which detects the current lap in game"""

    id = "LAP_INDICATOR"

    def get_information(self, image) -> dict:
        """Detect for each player what lap they currently are at

        :param image: In game image
        :type image: ndarray
        :return: A dictionary containing lap for each player
        :rtype: dict of (int, dict of (str, str))
        """

        assert 1 <= self.params["player_count"] <= 4

        n_players = self.params["player_count"]
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        imgs = in_game_stats(image, n_players, color=False, fields={LAP})
        stats = {}

        for p in range(1, n_players + 1):
            stats[p] = {LAP: None}

        for i, player in imgs.items():
            stats[i + 1][LAP] = detect_lap(player[LAP])

        return stats


def compute_img(image):
    _, th = cv2.threshold(image, 200, 255, cv2.THRESH_BINARY)
    return th


def match_lap(image, template, lap):
    computed_img = compute_img(image)

    if lap == 0:
        return np.array_equal(computed_img, template)

    result = cv2.norm(computed_img - template)

    return result < threshold


def detect_lap(image):
    laps = [0, 1, 2, 3]  # 0 means black, it means player is starting a new lap

    for lap in laps:
        template = cv2.imread(os.path.join(TEMPLATES_LAPS_IN_GAME_DIR, str(lap) + ".png"), cv2.IMREAD_GRAYSCALE)
        if match_lap(image, template, lap):
            return lap

    return None
