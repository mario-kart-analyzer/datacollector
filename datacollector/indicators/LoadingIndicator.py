import os

import cv2
from numpy.core.multiarray import ndarray

from datacollector.cutting.indicators.loading_indicator_cutter import crop_roi
from datacollector.definitions import TEMPLATES_LOADING_DIR
from datacollector.indicators import Indicator


class LoadingIndicator(Indicator.Indicator):
    """Loading indicator which detect whether we are loading"""

    id = "LOADING_INDICATOR"

    def __init__(self, params):
        super().__init__(params)
        template = cv2.imread(os.path.join(TEMPLATES_LOADING_DIR, "loading.png"), cv2.IMREAD_COLOR)
        template = cv2.resize(template, (0, 0), fx=0.25, fy=0.25)
        self.template_gray = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)

    def get_information(self, image: ndarray) -> bool:
        """Return True if we are loading

        :param image: In game image
        :type image: ndarray
        :return: True if we are loading
        :rtype: bool
        """

        roi = crop_roi(image)

        threshold = 8450000

        # resizing at 0.25 ratio
        image = cv2.resize(roi, (0, 0), fx=0.25, fy=0.25)

        # to gray
        image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        result = cv2.matchTemplate(image_gray, self.template_gray, cv2.TM_CCOEFF)
        minval, maxval, minloc, maxloc = cv2.minMaxLoc(result)

        return maxval > threshold
