import os

import cv2
from numpy.core.multiarray import ndarray

from datacollector.cutting.indicators.race_beginning_cutter import crop_roi
from datacollector.definitions import TEMPLATES_RACE_BEGINNING_DIR
from datacollector.indicators import Indicator


class RaceBeginningIndicator(Indicator.Indicator):
    """Indicator detecting when the race begins"""

    id = "RACE_BEGINNING_INDICATOR"

    def __init__(self, params):
        super().__init__(params)

        self.threshold = 6250000
        template = cv2.imread(os.path.join(TEMPLATES_RACE_BEGINNING_DIR, "begin4.png"), cv2.IMREAD_COLOR)
        template = cv2.resize(template, (0, 0), fx=0.25, fy=0.25)
        self.template = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)

    def get_information(self, image: ndarray) -> bool:
        """Return True if the race begin (according to 'start' message)
        
        :param image: In game image
        :type image: ndarray
        :return: Whether the race begins
        :rtype: bool
        """

        # resizing at 0.25 ratio the cropped image
        image = cv2.resize(crop_roi(image), (0, 0), fx=0.25, fy=0.25)
        result = cv2.matchTemplate(image, self.template, cv2.TM_CCOEFF)
        minval, maxval, minloc, maxloc = cv2.minMaxLoc(result)

        return maxval > self.threshold
