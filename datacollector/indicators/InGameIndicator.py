from numpy.core.multiarray import ndarray

from datacollector.indicators import Indicator


class InGameIndicator(Indicator.Indicator):
    """In game indicator class which detects whether we are in game"""

    id = "IN_GAME_INDICATOR"

    def get_information(self, image: ndarray) -> bool:
        """Return whether we are in game
        
        :param image: In game image
        :type image: ndarray
        :return: True if we are in game (in a race) else False
        :rtype: bool
        """

        # TODO : Implement
        return True
