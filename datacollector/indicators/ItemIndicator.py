import tensorflow as tf

import datacollector.learning.inception_v3 as inception
from datacollector.cutting.in_game_stats import in_game_stats
from datacollector.definitions import ITEM, ITEMS_NAMES, ITEMS_MODEL_CHECKPOINT, ITEMS_MODEL_GRAPH
from datacollector.indicators import Indicator


class ItemIndicator(Indicator.Indicator):
    """Indicator detecting items"""

    id = "ITEM_INDICATOR"

    def __init__(self, params, session, model):
        super().__init__(params)
        self.classifier = ItemClassifier(params["player_count"], session, model)

    def get_information(self, image) -> dict:
        """Detect for each player what item they have (or nothing)
        
        :param image: In game image
        :type image: ndarray
        :return: A dictionary containing items for each player
        :rtype: dict of (int, dict of (str, str))
        """

        assert 1 <= self.params["player_count"] <= 4

        n_players = self.params["player_count"]
        imgs = in_game_stats(image, n_players, color=True, fields={ITEM})
        stats = {}
        items_images = []

        for p in range(1, n_players + 1):
            stats[p] = {ITEM: None}
            items_images.append(imgs[p - 1][ITEM])

        items = self.classifier.get_item(items_images)

        for p in range(1, n_players + 1):
            stats[p][ITEM] = items[p - 1]

        return stats


class ItemClassifier(object):
    """Classify an image (which item it is) with Inception v3 model"""

    def __init__(self, player_count, session, model):
        assert 1 <= player_count <= 4

        self.model = model
        self.saver = None
        self.session = session
        self.x = None
        self.y_pred_cls = None
        self.player_count = player_count
        self.restore_graph()

    def restore_graph(self):
        self.saver = tf.train.import_meta_graph(ITEMS_MODEL_GRAPH)
        self.saver.restore(self.session, ITEMS_MODEL_CHECKPOINT)
        self.x = self.session.graph.get_tensor_by_name('items/x:0')
        self.y_pred_cls = self.session.graph.get_tensor_by_name('items/y_pred_cls:0')

    def get_item(self, images):
        """Return what item represent each image
        
        :param images: In game item images for each player
        :type images: list of ndarray
        :return: The predictions
        :rtype: list of str
        """

        assert self.player_count == len(images)

        transfer_values = inception.process_images(images=images, fn=self.model.transfer_values)
        predictions = self.session.run(self.y_pred_cls, {self.x: transfer_values})
        predictions = [ITEMS_NAMES[prediction] for prediction in predictions]

        return predictions
