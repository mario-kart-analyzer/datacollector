import tensorflow as tf

import datacollector.learning.inception_v3 as inception
from datacollector.cutting.in_game_stats import in_game_stats
from datacollector.definitions import POSITIONS_MODEL_CHECKPOINT, POSITIONS_MODEL_GRAPH, POSITIONS, RANK
from datacollector.indicators import Indicator


class PositionIndicator(Indicator.Indicator):
    """Indicator which detects the place in game"""

    id = "POSITION_INDICATOR"

    def __init__(self, params, session, model):
        super().__init__(params)
        self.classifier = PositionClassifier(params["player_count"], session, model)

    def get_information(self, image) -> dict:
        """Detect for each player what position they are at

        :param image: In game image
        :type image: ndarray
        :return: A dictionary containing ranks for each player
        :rtype: dict of (int, dict of (str, str))
        """

        assert 1 <= self.params["player_count"] <= 4

        n_players = self.params["player_count"]
        imgs = in_game_stats(image, n_players, color=True, fields={RANK})
        stats = {}
        ranks_images = []

        for p in range(1, n_players + 1):
            stats[p] = {RANK: None}
            ranks_images.append(imgs[p-1][RANK])

        ranks = self.classifier.get_position(ranks_images)

        for p in range(1, n_players + 1):
            stats[p][RANK] = ranks[p-1]

        return stats


class PositionClassifier:
    """Classify an image (which position it is) thanks to the Inception v3 model"""

    def __init__(self, player_count, session, model):
        assert 1 <= player_count <= 4

        self.model = model
        self.saver = None
        self.session = session
        self.x = None
        self.y_pred_cls = None
        self.player_count = player_count
        self.restore_graph()

    def restore_graph(self):
        self.saver = tf.train.import_meta_graph(POSITIONS_MODEL_GRAPH)
        self.saver.restore(self.session, POSITIONS_MODEL_CHECKPOINT)
        self.x = self.session.graph.get_tensor_by_name('positions/x:0')
        self.y_pred_cls = self.session.graph.get_tensor_by_name('positions/y_pred_cls:0')

    def get_position(self, images):
        """Return what position represent each image

        :param images: In game item images for each player
        :type images: list of ndarray
        :return: The predictions
        :rtype: list of str
        """

        assert self.player_count == len(images)

        transfer_values = inception.process_images(images=images, fn=self.model.transfer_values)
        predictions = self.session.run(self.y_pred_cls, {self.x: transfer_values})
        predictions = [POSITIONS[prediction] for prediction in predictions]

        return predictions

