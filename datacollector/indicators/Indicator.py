import abc

from numpy.core.multiarray import ndarray


class Indicator(abc.ABC):
    """Abstract indicator class"""

    id = None

    def __init__(self, params):
        self.params = params

    @abc.abstractclassmethod
    def get_information(self, image: ndarray):
        """Extract information relative to the indicator
        
        :param image: In game image
        :type image: ndarray
        :return: Information relative to the indicator
        """

        pass
