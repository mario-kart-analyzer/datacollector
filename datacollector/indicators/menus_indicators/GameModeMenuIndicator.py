import os

import cv2
from numpy.core.multiarray import ndarray

from datacollector.cutting.indicators.game_mode_menu_indicator_cutter import crop_roi
from datacollector.definitions import TEMPLATES_GAME_MODE_MENU_DIR
from datacollector.indicators.Indicator import Indicator


class GameModeMenuIndicator(Indicator):
    """Game mode menu indicator class which detects if whether we are in the game mode selection menu"""

    id = "GAME_MODE_MENU_INDICATOR"

    def __init__(self, params):
        super().__init__(params)

    def get_information(self, image: ndarray):
        """Detect if we are in the game mode menu
        
        :param image: In game image
        :type image: ndarray
        :return: True if we are in the game mode menu
        :rtype: bool
        """

        roi = crop_roi(image)
        threshold_versus = 2200000
        template = cv2.imread(os.path.join(TEMPLATES_GAME_MODE_MENU_DIR, "battle.png"), cv2.IMREAD_COLOR)
        result = self.get_result(roi, template)
        minval, maxval, minloc, maxloc = cv2.minMaxLoc(result)

        if maxval > threshold_versus:
            return True
        else:
            threshold_battle = 2200000
            template = cv2.imread(os.path.join(TEMPLATES_GAME_MODE_MENU_DIR, "versus_race.png"), cv2.IMREAD_COLOR)
            result = self.get_result(roi, template)
            minval, maxval, minloc, maxloc = cv2.minMaxLoc(result)

            return maxval > threshold_battle

    @staticmethod
    def get_result(roi, template):
        image = cv2.resize(roi, (0, 0), fx=0.25, fy=0.25)
        template = cv2.resize(template, (0, 0), fx=0.25, fy=0.25)

        image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        template_gray = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)

        return cv2.matchTemplate(image_gray, template_gray, cv2.TM_CCOEFF)
