from datacollector.indicators.menus_indicators.CarsMenuIndicator import CarsMenuIndicator
from datacollector.indicators.menus_indicators.CharacterSelectionIndicator import CharacterSelectionIndicator
from datacollector.indicators.menus_indicators.CupMenuIndicator import CupMenuIndicator
from datacollector.indicators.menus_indicators.DetailsMenuIndicator import DetailsMenuIndicator
from datacollector.indicators.menus_indicators.EndingScoreboardIndicator import EndingScoreboardIndicator
from datacollector.indicators.menus_indicators.GameModeMenuIndicator import GameModeMenuIndicator
from datacollector.indicators.menus_indicators.WelcomeMenuIndicator import WelcomeMenuIndicator


menu_indicator_list = {
    CarsMenuIndicator.id: CarsMenuIndicator,
    DetailsMenuIndicator.id: DetailsMenuIndicator,
    GameModeMenuIndicator.id: GameModeMenuIndicator,
    CupMenuIndicator.id: CupMenuIndicator,
    EndingScoreboardIndicator.id: EndingScoreboardIndicator,
    WelcomeMenuIndicator.id: WelcomeMenuIndicator,
    CharacterSelectionIndicator.id: CharacterSelectionIndicator
}
