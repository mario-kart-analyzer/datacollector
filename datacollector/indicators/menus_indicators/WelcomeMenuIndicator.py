import os

import cv2
from numpy.core.multiarray import ndarray

from datacollector.cutting.indicators.welcome_menu_indicator_cutter import crop_roi
from datacollector.definitions import TEMPLATES_WELCOME_MENU_DIR
from datacollector.indicators import Indicator


class WelcomeMenuIndicator(Indicator.Indicator):
    """Welcome menu indicator class which detects if whether we are in the welcome menu"""

    id = "WELCOME_MENU_INDICATOR"

    def get_information(self, image: ndarray) -> bool:
        """Detect if we are at the welcome menu
        
        :param image: In game image
        :type image: ndarray
        :return: True if we are at the welcome menu
        :rtype: bool
        """

        roi = crop_roi(image)
        threshold = 1350000
        template = cv2.imread(os.path.join(TEMPLATES_WELCOME_MENU_DIR, "MarioKartTV.png"), cv2.IMREAD_COLOR)
        result = self.get_result(roi, template)
        minval, maxval, minloc, maxloc = cv2.minMaxLoc(result)

        if maxval > threshold:
            return True
        else:
            roi = crop_roi(image)
            threshold2 = 2250000
            template = cv2.imread(os.path.join(TEMPLATES_WELCOME_MENU_DIR, "MultiLocal.png"), cv2.IMREAD_COLOR)
            result = self.get_result(roi, template)
            minval, maxval, minloc, maxloc = cv2.minMaxLoc(result)

            return maxval > threshold2

    @staticmethod
    def get_result(roi, template):
        image = cv2.resize(roi, (0, 0), fx=0.25, fy=0.25)
        template = cv2.resize(template, (0, 0), fx=0.25, fy=0.25)

        image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        template_gray = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)

        return cv2.matchTemplate(image_gray, template_gray, cv2.TM_CCOEFF)
