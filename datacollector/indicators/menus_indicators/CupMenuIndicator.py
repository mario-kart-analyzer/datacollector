import os

import cv2
from numpy.core.multiarray import ndarray

from datacollector.cutting.indicators.cup_menu_indicator_cutter import crop_roi
from datacollector.definitions import TEMPLATES_CUP_MENU_DIR
from datacollector.indicators.Indicator import Indicator


class CupMenuIndicator(Indicator):
    """Cup menu indicator class which detects if whether we are in the cup selection menu"""

    id = "CUP_MENU_INDICATOR"

    def __init__(self, params):
        super().__init__(params)

        template_champ = cv2.imread(os.path.join(TEMPLATES_CUP_MENU_DIR, "champignon.png"), cv2.IMREAD_COLOR)
        template_champ = cv2.resize(template_champ, (0, 0), fx=0.25, fy=0.25)
        self.template_gray_champ = cv2.cvtColor(template_champ, cv2.COLOR_BGR2GRAY)

        template_carap = cv2.imread(os.path.join(TEMPLATES_CUP_MENU_DIR, "carapace_verte.png"), cv2.IMREAD_COLOR)
        template_carap = cv2.resize(template_carap, (0, 0), fx=0.25, fy=0.25)
        self.template_gray_carap = cv2.cvtColor(template_carap, cv2.COLOR_BGR2GRAY)

    def get_information(self, image: ndarray):
        """Detect if we are in the cup selection menu
        
        :param image: In game image
        :type image: ndarray
        :return: True if we are in the menu
        :rtype: bool
        """

        roi = crop_roi(image)
        threshold = 6800000

        # resizing at 0.25 ratio
        image = cv2.resize(roi, (0, 0), fx=0.25, fy=0.25)

        # to gray
        image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        result = cv2.matchTemplate(image_gray, self.template_gray_champ, cv2.TM_CCOEFF)
        minval, maxval, minloc, maxloc = cv2.minMaxLoc(result)

        if maxval > threshold:
            return True
        else:
            # resizing at 0.25 ratio
            image = cv2.resize(roi, (0, 0), fx=0.25, fy=0.25)

            # to gray
            image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

            result = cv2.matchTemplate(image_gray, self.template_gray_carap, cv2.TM_CCOEFF)
            minval, maxval, minloc, maxloc = cv2.minMaxLoc(result)

            return maxval > threshold
