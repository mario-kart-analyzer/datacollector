import cv2
import numpy as np
from numpy.core.multiarray import ndarray

from datacollector.cutting.indicators.car_menu_indicator.multi import crop_rois
from datacollector.definitions import PLAYER_COLORS
from datacollector.indicators.Indicator import Indicator


def detect_players(image):
    """Detect the number of players
    
    :param image: In game image
    :type image: ndarray
    :return: True for each player if he is detected
    :rtype: dict of (int, bool)
    """

    multi_roi = crop_rois(image)

    # Precomputed color means for each player

    threshold = {
        1: 2,
        2: 2,
        3: 2,
        4: 2
    }

    players_detected = {
    }

    for i, roi in multi_roi.items():
        lab = cv2.cvtColor(roi, cv2.COLOR_RGB2LAB)

        if np.linalg.norm(cv2.mean(lab) - PLAYER_COLORS[i]) < threshold[i]:
            players_detected[i] = True
        else:
            players_detected[i] = False

    return players_detected


class CarsMenuIndicator(Indicator):
    """Car menu indicator class which detects if whether we are in the car selection menu"""

    id = "CARS_MENU_INDICATOR"

    def get_information(self, image: ndarray):
        """Detect if whether we are in the car selection menu, and the number of players
        
        :param image: In game image
        :type image: ndarray
        :return: A dictionary containing if we are in the menu and the number of players if we are
        :rtype: dict of (str, ?)
        """

        players_detected = detect_players(image)
        n_players_detected = sum(players_detected.values())

        if n_players_detected == 0:
            return {
                "in_menu": False
            }

        return {
            "in_menu": True,
            "players_detected": players_detected,
            "n_players_detected": n_players_detected
        }
