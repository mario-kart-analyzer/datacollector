import cv2
import numpy as np
from numpy.core.multiarray import ndarray

from datacollector.cutting.indicators.scoreboard_cutter import crop_roi
from datacollector.definitions import RANKS_SCORES
from datacollector.indicators import Indicator


def _find_pos_by_player(scoreboard, player):
    for pos in scoreboard.keys():
        if scoreboard[pos]["type"] == player:
            return pos


def _check_first_race(scoreboard):
    for _, v in scoreboard.items():
        if v["type"][:2] != "ai":
            return False

    return True


class ScoreboardIndicator(Indicator.Indicator):
    """Scoreboard indicator (at the end of each race)"""

    id = "SCOREBOARD_INDICATOR"

    def __init__(self, params):
        super().__init__(params)
        self.boundaries = {
            "player1": (np.array([19, 100, 100], dtype=np.uint8), np.array([39, 255, 255], dtype=np.uint8)),
            "player2": (np.array([168, 100, 100], dtype=np.uint8), np.array([188, 255, 255], dtype=np.uint8)),
            "player3": (np.array([79, 100, 100], dtype=np.uint8), np.array([99, 255, 255], dtype=np.uint8)),
            "player4": (np.array([36, 100, 100], dtype=np.uint8), np.array([56, 255, 255], dtype=np.uint8))
        }  # cv2.inRange asks for numpy arrays with uint8 values (not lists)

        self.player_count = params["player_count"]
        self.scoreboard = params["scoreboard"]
        self.sb_copy = self.scoreboard.copy()
        self.lowest = {str("player%d" % s): [-1, 1.0] for s in range(1, self.player_count+1)}

    def get_information(self, image: ndarray) -> dict:
        """Return a dictionary with at each position if it is an AI or an human

        :param image: In game image
        :type image: ndarray
        :return: A dictionary summary for each position
        :rtype: dict of (int, str)
        """

        hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        players = crop_roi(hsv)

        for i in range(1, 13):
            pixel_count = len(players[i][0]) * len(players[i])
            for k, (lower, upper) in self.boundaries.items():
                mask = cv2.inRange(players[i], lower, upper)
                res = cv2.bitwise_and(players[i], players[i], mask=mask)
                grayscale = cv2.cvtColor(res, cv2.COLOR_BGR2GRAY)

                black_pixels_ratio = float((pixel_count - cv2.countNonZero(grayscale)) / pixel_count)
                if int(k[-1]) <= self.player_count and self.lowest[k][1] > black_pixels_ratio:
                    self.lowest[k] = [i, black_pixels_ratio]

        # as there are, depending on the background, sometimes multiple detection of a color
        # (like yellow in 'Piste aux délices'), we take the lowest ratio for each player color

        if _check_first_race(self.scoreboard):
            for player in self.lowest.keys():
                pos = self.lowest[player][0]
                self.scoreboard[pos]["type"] = player
                self.scoreboard[pos]["score"] += RANKS_SCORES[pos]
        else:
            new_positions = []
            for player in self.lowest.keys():
                new_pos = self.lowest[player][0]
                new_positions.append(new_pos)
                old_pos = _find_pos_by_player(self.sb_copy, player)
                self.scoreboard[old_pos]["score"] += RANKS_SCORES[new_pos]  # first we increase each player's score

            for player in self.lowest.keys():
                old_pos = _find_pos_by_player(self.sb_copy, player)
                new_pos = self.lowest[player][0]
                self.scoreboard[new_pos] = self.scoreboard[old_pos]  # then we move them to the corresponding position

            for player in self.lowest.keys():
                old_pos = _find_pos_by_player(self.sb_copy, player)
                if old_pos not in new_positions:
                    self.scoreboard[old_pos] = {"type": "ai", "score": 0}  # finally we replace old positions with ai

        return self.scoreboard
