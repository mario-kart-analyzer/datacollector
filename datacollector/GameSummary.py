from decimal import Decimal


class GameSummary(object):
    """Summary of a game object"""

    def __init__(self):
        """Instantiate a summary"""

        self.race_name = ""
        self.scoreboard = {}
        self.players = {}

    def digest(self, game):
        """Digest relevant information of a game in order to save it
        
        :param game: Current game
        :type game: Game
        """

        self.race_name = game.current_race
        self.scoreboard = game.last_scoreboard

        for player_id in game.players.keys():
            player_summary = PlayerSummary()
            player_summary.digest(game.players[player_id])
            self.players[player_id] = player_summary


class PlayerSummary(object):
    """Summary of a player object"""

    def __init__(self):
        """Instantiate a summary"""

        self.score = 0
        self.race_time = 0
        self.position = 0
        self.laps_time = []
        self.items_count = {}

    def digest(self, player):
        """Digest relevant information of a player in order to save it

        :param game: Current player
        :type game: Player
        """

        self.score = player.score
        self.position = player.race_position
        self.items_count = player.items_count
        self.race_time = float(round(Decimal(player.race_times["total"]), 3))  # round it to 3 decimal points
        self.laps_time = player.race_times["laps"]

        for i, lap_time in enumerate(self.laps_time):
            self.laps_time[i] = float(round(Decimal(lap_time), 3))  # round it to 3 decimal points
