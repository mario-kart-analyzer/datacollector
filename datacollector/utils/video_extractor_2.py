import os
import re
from os.path import isfile, join, exists

import cv2

from datacollector.definitions import IMAGE_DIR, VIDEO_DIR, IMG_SIZE, LAP_COORDS, IMG_LAP_SIZE
from datacollector.indicators.LapIndicator import compute_img


def get_max_index_in(images_success_path):
    max_index = 0
    files = [f for f in os.listdir(images_success_path) if isfile(join(images_success_path, f))]

    for file in files:
        p = re.compile('^(\d+)_')
        m = p.match(file)
        if m:
            index = int(m.group(1))
            max_index = max(max_index, index)

    return max_index

def extract(video_file_name, images_subfolder, ROIs=[], resize=True, size=IMG_SIZE, treatment=lambda x: x):
    min_time_between_img = 1000

    video_path = join(VIDEO_DIR, video_file_name + ".mp4")
    images_success_path = join(images_subfolder, "success")
    images_failure_path = join(images_subfolder, "failure")

    video = cv2.VideoCapture(video_path)

    if not video.isOpened():
        print("Cannot open video")
        return

    if not exists(images_success_path):
        os.makedirs(images_success_path)

    if not exists(images_failure_path):
        os.makedirs(images_failure_path)

    pause = False
    i_success = get_max_index_in(images_success_path) + 1
    i_failure = get_max_index_in(images_success_path) + 1

    last_img_time = video.get(0)

    ret, frame = video.read()
    frame = cv2.resize(frame, IMG_SIZE)

    while True:
        if not pause:
            ret, frame = video.read()
            frame = cv2.resize(frame, IMG_SIZE)

        if ret is False:
            break

        if len(ROIs) == 0:
            ROIs.append((0, 1, 0, 1))

        cv2.imshow("frame", frame)

        for i, roi in enumerate(ROIs):
            x_start, x_end, y_start, y_end = roi
            image_path = join(images_failure_path, str(i_failure) + '_' + str(i) + '.png')
            frame_part = frame[int(IMG_SIZE[1] * y_start): int(IMG_SIZE[1] * y_end),
                         int(IMG_SIZE[0] * x_start): int(IMG_SIZE[0] * x_end)]
            cv2.imshow("img", cv2.resize(treatment(frame_part), size))

        key = cv2.waitKey(1)

        if key == ord('q'):  # Quit
            break

        if key == ord('p'):  # Pause
            pause = not pause
            continue

        time_since_last_img = video.get(0) - last_img_time

        if time_since_last_img <= min_time_between_img:
            continue

        if key == ord('s'):  # Add to success

            for i, roi in enumerate(ROIs):
                x_start, x_end, y_start, y_end = roi
                image_path = join(images_success_path, str(i_success) + '_' + str(i) + '.png')
                frame_part = frame[int(IMG_SIZE[1] * y_start): int(IMG_SIZE[1] * y_end),
                             int(IMG_SIZE[0] * x_start): int(IMG_SIZE[0] * x_end)]

            cv2.imwrite(image_path, cv2.resize(treatment(frame_part), size))
            i_success += 1

            last_img_time = video.get(0)

        if key == ord('f'):  # Add to failure
            for i, roi in enumerate(ROIs):
                x_start, x_end, y_start, y_end = roi
                image_path = join(images_failure_path, str(i_failure) + '_' + str(i) + '.png')
                frame_part = frame[int(IMG_SIZE[1] * y_start): int(IMG_SIZE[1] * y_end),
                             int(IMG_SIZE[0] * x_start): int(IMG_SIZE[0] * x_end)]

            cv2.imwrite(image_path, cv2.resize(treatment(frame_part), size))
            i_failure += 1

            last_img_time = video.get(0)

    # When everything done, release the capture
    video.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    name = "ingame"
    folder = os.path.join(IMAGE_DIR, "lap")

    roi = [coord / 2 for coord in LAP_COORDS]

    extract(name, folder, [roi], False, IMG_LAP_SIZE,
            lambda img: compute_img(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)))
