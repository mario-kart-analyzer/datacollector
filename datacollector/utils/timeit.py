import time


def timeit(method):
    """
    Attribute timing a method

    Parameters
    ----------
    method : method
        the method to time

    Returns
    -------
    object
        the result of the method
    """

    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()

        print('%r %2.2f sec : %s' %
              (method.__name__, te - ts, result))
        return result

    return timed
