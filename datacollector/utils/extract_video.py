import argparse
import os
from os.path import basename, splitext

import cv2

from datacollector.definitions import VIDEO_DIR, IMAGE_DIR

parser = argparse.ArgumentParser(description='')

parser.add_argument('video_file_name', type=str, help='Input video file name (in videos folder)')
parser.add_argument('--images_subfolder', type=str, help='Output images folder name, default is the video name')
parser.add_argument('--period', default=1000, type=int, help='Set period between frame extraction, default 1000ms')


def extract_video(video_file_name, folder, period=1000):
    """
    Extract frames from the video every 'period' milliseconds

    Parameters
    ----------
    video_file_name : str
        the file name of the video
    images_subfolder : str
        the path where images are stocked
    period : int
        the period in milliseconds (default 1000)
    """

    video_path = os.path.join(VIDEO_DIR, video_file_name + ".mp4")
    images_path = os.path.join(IMAGE_DIR, folder)

    print(video_path)

    video = cv2.VideoCapture(video_path)

    if not video.isOpened():
        print("Cannot open video")
        return

    if not os.path.exists(images_path):
        os.makedirs(images_path)

    last_img_time = video.get(0)
    i = 0

    while True:
        ret, frame = video.read()

        if ret is False:
            break

        time_since_last_img = video.get(0) - last_img_time

        if time_since_last_img >= period:
            image_path = os.path.join(images_path, str(i) + '.png')
            cv2.imwrite(image_path, frame)
            last_img_time = video.get(0)

            i += 1

    # When everything done, release the capture
    video.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    args = parser.parse_args()

    if args.images_subfolder is None:
        folder = splitext(basename(args.video_file_name))[0]
    else:
        folder = args.images_subfolder

    extract_video(args.video_file_name, folder, period=args.period)
