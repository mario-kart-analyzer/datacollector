from os.path import join

import cv2

from datacollector.definitions import VIDEO_DIR, IMG_SIZE


def give_coords(video_file_name):
    """
    Print in the standard output coordinates of the selected region

    Parameters
    ----------
    video_file_name : str
        the file name of the video to play
    """

    video_path = join(VIDEO_DIR, video_file_name + ".mp4")

    video = cv2.VideoCapture(video_path)

    if not video.isOpened():
        print("Cannot open video")
        return

    cv2.namedWindow("image")
    cv2.setMouseCallback("image", click_handler)

    while True:
        ret, frame = video.read()
        if ret is False:
            break

        frame = cv2.resize(frame, IMG_SIZE)
        cv2.imshow("image", frame)
        key = cv2.waitKey(1)

    # When everything done, release the capture
    video.release()
    cv2.destroyAllWindows()


def click_handler(event, x, y, flags, param):
    """
    Handle the click event by printing the coordinates
    """

    if event == cv2.EVENT_LBUTTONDOWN:
        print(x/IMG_SIZE[0], y/IMG_SIZE[1])


if __name__ == '__main__':
    name = "ingame"
    give_coords(name)
