import os

import cv2


def main(videos_dir):
    video_index = 0

    for (_, _, files) in os.walk(videos_dir):
        print("Cropping the following videos: %s" % files)
        break

    for video in files:
        path = os.path.join(videos_dir, video)
        print("Beginning %s" % path)
        cap = cv2.VideoCapture(path)
        fourcc = cv2.VideoWriter_fourcc(*'MP4V')
        out = cv2.VideoWriter('../../data/videos/train_dataset/positions/0-' + str(video_index) + '.mp4',
                              fourcc, 30.0, (100, 100))  # (125, 125) for item, (100, 100) for rank

        while cap.isOpened():
            ret, frame = cap.read()

            if ret:
                height, width, _ = frame.shape
                # ranks
                # frame = frame[425:525, 810:910]  # first player
                # frame = frame[960:1060, 810:910]  # second player
                # frame = frame[420:520, 1770:1870]  # third player
                frame = frame[550:710, 1050:1210]  # single player on elgato recording

                # items
                # frame = frame[20:145, 30:155]  # first player
                # frame = frame[560:685, 30:155]  # second player
                # frame = frame[20:145, 990:1115]  # third player
                # frame = frame[40:205, 65:225]  # single player on elgato recording

                frame = cv2.resize(frame, (100, 100))  # (125, 125) for item, (100, 100) for rank

                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break

                out.write(frame)
            else:
                break

        cap.release()
        out.release()

        video_index += 1

if __name__ == '__main__':
    main("..\\..\\data\\videos\\train_dataset\\positions")
