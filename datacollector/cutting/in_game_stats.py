import cv2
from numpy.core.multiarray import ndarray

from datacollector.cutting.split_screen import split_screen
from datacollector.definitions import RANK, COINS, LAP, ITEM, IMG_2_PLAYERS_SIZE, IMG_4_PLAYERS_SIZE, \
    IMG_RANK_SIZE, IMG_LAP_SIZE, LAP_COORDS, IMG_3_PLAYERS_SIZE, IMG_ITEM_SIZE


def in_game_stats(image: ndarray, player_count: int, color: bool, fields={RANK, COINS, LAP, ITEM}) -> dict:
    """Crop images according to the fields desired and to the number of players in game
    
    :param image: In game image
    :param player_count: Number of players
    :param color: If false, the image is in greyscale, else it's in color
    :param fields: Informations asked (such as ranking, coins, lap, item)
    :type image: ndarray
    :type n_players: int
    :type color: bool
    :type fields: set of str
    :return: Images of asked fields for each player
    :rtype: dict of (int, ndarray)
    """

    stats = {}

    players_views = split_screen(image, player_count, color=color)

    for i in range(0, player_count):
        player_stats = {}
        player_image = players_views[i]

        if player_count == 1:
            if RANK in fields:
                player_stats[RANK] = extract_rank_1_player(player_image)
            if COINS in fields:
                player_stats[COINS] = extract_coins_1_player(player_image)
            if LAP in fields:
                player_stats[LAP] = extract_lap_1_player(player_image)
            if ITEM in fields:
                player_stats[ITEM] = extract_item_1_player(player_image)

        if player_count == 2:
            if RANK in fields:
                player_stats[RANK] = extract_rank_2_player(player_image)
            if COINS in fields:
                player_stats[COINS] = extract_coins_2_player(player_image)
            if LAP in fields:
                player_stats[LAP] = extract_lap_2_player(player_image)
            if ITEM in fields:
                player_stats[ITEM] = extract_item_2_player(player_image)

        if player_count == 3:
            if RANK in fields:
                player_stats[RANK] = extract_rank_3_player(player_image)
            if COINS in fields:
                player_stats[COINS] = extract_coins_3_player(player_image)
            if LAP in fields:
                player_stats[LAP] = extract_lap_3_player(player_image)
            if ITEM in fields:
                player_stats[ITEM] = extract_item_3_player(player_image)

        if player_count == 4:
            if RANK in fields:
                player_stats[RANK] = extract_rank_4_player(player_image)
            if COINS in fields:
                player_stats[COINS] = extract_coins_4_player(player_image)
            if LAP in fields:
                player_stats[LAP] = extract_lap_4_player(player_image)
            if ITEM in fields:
                player_stats[ITEM] = extract_item_4_player(player_image)

        stats[i] = player_stats

    return stats


def extract_rank_1_player(image):
    return image


def extract_coins_1_player(image):
    return image


def extract_lap_1_player(image):
    return image


def extract_item_1_player(image):
    return image


def extract_rank_2_player(image):
    cols, rows = IMG_2_PLAYERS_SIZE

    rank_x_start = int(rows*0.85)
    rank_x_end = int(rows*0.96)

    rank_y_start = int(cols*0.69)
    rank_y_end = int(cols*0.91)

    rank = cv2.resize(image[rank_x_start:rank_x_end, rank_y_start:rank_y_end], IMG_RANK_SIZE)
    return rank


def extract_coins_2_player(image):
    return image


def extract_lap_2_player(image):
    cols, rows = IMG_2_PLAYERS_SIZE

    rank_x_start = int(cols * LAP_COORDS[0])
    rank_x_end = int(cols * LAP_COORDS[1])
    rank_y_start = int(rows * LAP_COORDS[2])
    rank_y_end = int(rows * LAP_COORDS[3])

    rank = cv2.resize(image[rank_y_start:rank_y_end, rank_x_start:rank_x_end], IMG_LAP_SIZE)
    return rank


def extract_item_2_player(image):
    return image


def extract_rank_3_player(image):
    return extract_rank_4_player(image)


def extract_coins_3_player(image):
    return image


def extract_lap_3_player(image):
    cols, rows = IMG_3_PLAYERS_SIZE

    rank_x_start = int(cols * LAP_COORDS[0])
    rank_x_end = int(cols * LAP_COORDS[1])
    rank_y_start = int(rows * LAP_COORDS[2])
    rank_y_end = int(rows * LAP_COORDS[3])

    rank = cv2.resize(image[rank_y_start:rank_y_end, rank_x_start:rank_x_end], IMG_LAP_SIZE)
    return rank


def extract_item_3_player(image):
    return extract_item_4_player(image)


def extract_rank_4_player(image):
    rank_y_start = 810
    rank_y_end = 910
    rank_x_start = 425
    rank_x_end = 525

    rank = cv2.resize(image[rank_x_start:rank_x_end, rank_y_start:rank_y_end], IMG_RANK_SIZE)
    return rank


def extract_coins_4_player(image):
    return image


def extract_lap_4_player(image):
    cols, rows = IMG_4_PLAYERS_SIZE

    rank_x_start = int(cols*LAP_COORDS[0])
    rank_x_end = int(cols*LAP_COORDS[1])
    rank_y_start = int(rows*LAP_COORDS[2])
    rank_y_end = int(rows*LAP_COORDS[3])

    lap = cv2.resize(image[rank_y_start:rank_y_end, rank_x_start:rank_x_end], IMG_LAP_SIZE)
    return lap


def extract_item_4_player(image):
    item_y_start = 20
    item_y_end = 145
    item_x_start = 30
    item_x_end = 155

    item = cv2.resize(image[item_y_start:item_y_end, item_x_start:item_x_end], IMG_ITEM_SIZE)
    return item
