from numpy.core.multiarray import ndarray


def crop_roi(image: ndarray):
    """Crop regions of interest for the scoreboard indicator

    :param image: In game image
    :type image: ndarray
    :return: Cropped images
    :rtype: list of ndarray
    """

    scoreboard = image[70:1010, 450:1470]
    players = {k: [] for k in range(1, 13)}

    for i in range(1, 13):
        players[i] = scoreboard[78*(i-1):78*i, :]

    return players
