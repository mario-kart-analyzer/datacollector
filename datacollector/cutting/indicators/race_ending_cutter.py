import cv2
from numpy.core.multiarray import ndarray

from datacollector.cutting.split_screen import split_screen


def crop_roi(image: ndarray, player_count: int) -> list:
    """Crop region of interest for the race ending indicator
    
    :param image: In game image
    :param player_count: Number of players
    :type image: ndarray
    :type player_count: int
    :return: Cropped images for comparison with race ending's template
    :rtype: list of ndarray
    """

    image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    if player_count == 1:
        players_images = [image_gray[175:300, 175:775]]
    else:
        players_images = split_screen(image_gray, player_count)
        if player_count == 2:
            players_images[0] = players_images[0][410:570, 70:880]
            players_images[1] = players_images[1][410:570, 70:880]
        else:
            for i in range(0, player_count):
                players_images[i] = players_images[i][175:300, 175:775]

    return players_images
