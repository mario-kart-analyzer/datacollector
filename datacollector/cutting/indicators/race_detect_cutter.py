import cv2

from numpy.core.multiarray import ndarray


def crop_roi(image: ndarray):
    """Crop region of interest for the race detection indicator
    
    :param image: In game image
    :type image: ndarray
    :return: Cropped and threshold applied to the image for OCR reading
    :rtype: ndarray
    """

    image_cropped = image[925:1000, 520:1400]
    _, th = cv2.threshold(image_cropped, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

    return th
