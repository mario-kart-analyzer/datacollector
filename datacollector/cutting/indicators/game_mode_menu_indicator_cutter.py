from numpy.core.multiarray import ndarray


def crop_roi(image: ndarray):
    """Crop region of interest for the game mode menu indicator
    
    :param image: In game image
    :type image: ndarray
    :return: Cropped image for comparison with game mode menu's template
    :rtype: ndarray
    """

    return image[420:730, 130:930]
