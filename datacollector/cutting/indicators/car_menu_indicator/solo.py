from numpy.core.multiarray import ndarray


def crop_roi(image: ndarray):
    """Crop region of interest the for car menu indicator (solo)
    
    :param image: In game image
    :type image: ndarray
    :return: Cropped image for comparison with car menu's template
    :rtype: ndarray
    """

    return image[150:880, 0:850]
