from numpy.core.multiarray import ndarray

from datacollector.definitions import IMG_SIZE


def crop_rois(image: ndarray) -> dict:
    """Crop regions of interest for the car menu indicator (multi)
    
    :param image: In game image
    :type image: ndarray
    :return: A dictionary containing the cropped ROIs for each player
    :rtype: dict of (int, ndarray)
    """

    col1 = int(IMG_SIZE[0] * 0.37)
    col2 = int(IMG_SIZE[0] * 0.62)

    row1 = int(IMG_SIZE[1] * 0.083)
    row2 = int(IMG_SIZE[1] * 0.5)

    crop_size_x = 180
    crop_size_y = 16

    crop_player_1 = image[
                    int(row1 - crop_size_y / 2): int(row1 + crop_size_y / 2),
                    int(col1 - crop_size_x / 2): int(col1 + crop_size_x / 2)]

    crop_player_2 = image[
                    int(row1 - crop_size_y / 2): int(row1 + crop_size_y / 2),
                    int(col2 - crop_size_x / 2): int(col2 + crop_size_x / 2)]

    crop_player_3 = image[
                    int(row2 - crop_size_y / 2): int(row2 + crop_size_y / 2),
                    int(col1 - crop_size_x / 2): int(col1 + crop_size_x / 2)]

    crop_player_4 = image[
                    int(row2 - crop_size_y / 2): int(row2 + crop_size_y / 2),
                    int(col2 - crop_size_x / 2): int(col2 + crop_size_x / 2)]

    return {
        1: crop_player_1,
        2: crop_player_2,
        3: crop_player_3,
        4: crop_player_4
    }
