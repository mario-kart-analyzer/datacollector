from numpy.core.multiarray import ndarray


def crop_roi(image: ndarray):
    """Crop region of interest for the character selection indicator
    
    :param image: In game image
    :type image: ndarray
    :return: Cropped image for comparison with character selection's template
    :rtype: ndarray
    """

    return image[45:180, 500:630]
