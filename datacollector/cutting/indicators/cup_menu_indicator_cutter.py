import cv2
from numpy.core.multiarray import ndarray


def crop_roi(image: ndarray):
    """Crop region of interest for the cup menu indicator
    
    :param image: In game image
    :type image: ndarray
    :return: Cropped image for comparison with cup menu's template
    :rtype: ndarray
    """
    image = cv2.resize(image, (1920, 1080))
    return image[0:600, 0:700]
