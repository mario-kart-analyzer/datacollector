from numpy.core.multiarray import ndarray


def crop_roi(image: ndarray):
    """Crop region of interest for the welcome menu indicator
    
    :param image: In game image
    :type image: ndarray
    :return: Cropped image for comparison with character selection's template
    :rtype: ndarray
    """

    return image[150:880, 0:850]
