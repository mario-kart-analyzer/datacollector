import cv2
from numpy.core.multiarray import ndarray


def crop_roi(image: ndarray):
    """Crop region of interest for the race beginning indicator
    
    :param image: In game image
    :type image: ndarray
    :return: Cropped image for comparison with race beginning's template
    :rtype: ndarray
    """

    image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    return image_gray[350:600, 300:1500]
