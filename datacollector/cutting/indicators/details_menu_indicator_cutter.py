from numpy.core.multiarray import ndarray


def crop_roi(image: ndarray):
    """Crop region of interest for the details menu indicator
    
    :param image: In game image
    :type image: ndarray
    :return: Cropped image for comparison with details menu's template
    :rtype: ndarray
    """

    return image[0:175, 300:750]
