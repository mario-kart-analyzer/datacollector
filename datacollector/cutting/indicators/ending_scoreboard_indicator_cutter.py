from numpy.core.multiarray import ndarray


def crop_roi(image: ndarray):
    """Crop region of interest for the ending scoreboard indicator
    
    :param image: In game image
    :type image: ndarray
    :return: Cropped image for comparison with ending scoreboard's template
    :rtype: ndarray
    """

    return image[70:180, 140:890]
