import cv2
from numpy.core.multiarray import ndarray


def crop_roi(image: ndarray):
    """Crop region of interest for the loading indicator
    
    :param image: In game image
    :type image: ndarray
    :return: Cropped image for comparison with loading's template
    :rtype: ndarray
    """

    image = cv2.resize(image, (1920, 1080))
    return image[700:1080, 0:1920]
