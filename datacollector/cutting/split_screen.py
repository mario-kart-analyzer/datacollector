from numpy import ndarray


def split_screen(image: ndarray, n_players: int, color=False) -> list:
    """Split the screen into images corresponding to the play screen for each player
    
    :param image: In game image
    :param n_players: Number of players
    :param color: If false, the image is in greyscale, else it's in color
    :type image: ndarray
    :type n_players: int
    :type color: bool
    :return: The image cut for each player, in a list (or alone if there's only one player)
    :rtype: list of ndarray
    """

    assert 1 <= n_players <= 4, "Incorrect number of player : %d" % n_players

    if color:
        rows, cols, _ = image.shape
    else:
        rows, cols = image.shape

    if n_players == 1:
        return [image]

    if n_players == 2:
        return [
            image[:, int(cols / 2):],
            image[:, :int(cols / 2)],
        ]

    if n_players == 3:
        return [
            image[:int(rows / 2), :int(cols / 2)],
            image[int(rows / 2):, :int(cols / 2)],
            image[:int(rows / 2), int(cols / 2):]
        ]

    if n_players == 4:
        return [
            image[:int(rows / 2), :int(cols / 2)],
            image[int(rows / 2):, :int(cols / 2)],
            image[:int(rows / 2), int(cols / 2):],
            image[int(rows / 2):, int(cols / 2):]
        ]
