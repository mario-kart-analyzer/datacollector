import abc
import uuid


class IndicatorResponseCorrector(abc.ABC):
    """Class correcting the responses of indicators (abstract)"""

    def __init__(self, context, indicator_id, params):
        """Instantiate with parameters
        
        :param context: The game context
        :param indicator_id: The id identifying the indicator
        :param params: Parameters passed to the corrector
        """
        self.default_params = {}
        self.params = {**self.default_params, **params}
        self.uuid = uuid.uuid4()
        self.indicator_id = indicator_id
        self.context = context

    @abc.abstractmethod
    def update_value(self, indicator_response): pass

    @abc.abstractproperty
    def value(self): pass
