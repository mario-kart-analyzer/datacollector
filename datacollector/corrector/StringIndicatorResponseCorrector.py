from datacollector.corrector.IndicatorResponseCorrector import IndicatorResponseCorrector

THRESHOLD = "threshold"
POSITIVE_IMPACT = "positive_impact"
NEGATIVE_IMPACT = "negative_impact"
DEFAULT_VALUE = "default_value"
LOG = "log"

DETECT_STRING = {
    POSITIVE_IMPACT: 0.2,
    NEGATIVE_IMPACT: 0.2
}


class StringIndicatorResponseCorrector(IndicatorResponseCorrector):
    """Class correcting the responses of indicators (of type str)"""

    def __init__(self, context, indicator_id, params=DETECT_STRING):
        """Instantiate with parameters

        :param context: The game context
        :param indicator_id: The id identifying the indicator
        :param params: Parameters passed to the corrector
        """

        super().__init__(context, indicator_id, params)
        self._values = {}

    def update_value(self, indicator_response):
        """Update the current value according to positive and negative impacts
        
        :param indicator_response: Value of the response
        :type indicator_response: str
        """

        if type(indicator_response) is not str:
            return

        if indicator_response in self._values:
            self._values[indicator_response] += self.params[POSITIVE_IMPACT]
            self._values[indicator_response] = min(1, self._values[indicator_response])
        else:
            self._values[indicator_response] = self.params[POSITIVE_IMPACT]

        self._values = {k: self.reduce_value(k, indicator_response) for k, v in self._values.items()}

    @property
    def value(self):
        """Return value according to the corrector's threshold
        
        :return: The most impactful string based on the parameters values
        :rtype: str
        """

        return max(self._values.keys(), key=(lambda key: self._values[key])) if self._values else None

    def reduce_value(self, key, exception):
        return self._values[key] if key == exception else max(0, self._values[key] - self.params[NEGATIVE_IMPACT])
