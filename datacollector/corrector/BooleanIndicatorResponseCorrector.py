import logging

import cv2

from datacollector.corrector.IndicatorResponseCorrector import IndicatorResponseCorrector

THRESHOLD = "threshold"
POSITIVE_IMPACT = "positive_impact"
NEGATIVE_IMPACT = "negative_impact"
DEFAULT_VALUE = "default_value"
LOG = "log"

DETECT_NEGATIVE = {
    THRESHOLD: 0.2,
    POSITIVE_IMPACT: 0.2,
    NEGATIVE_IMPACT: 0.2,
    DEFAULT_VALUE: True,
    LOG: False
}

DETECT_POSITIVE = {
    THRESHOLD: 0.8,
    POSITIVE_IMPACT: 0.2,
    NEGATIVE_IMPACT: 0.2,
    DEFAULT_VALUE: False,
    LOG: False
}

DETECT_QUICK_POSITIVE = {
    THRESHOLD: 0.8,
    POSITIVE_IMPACT: 0.4,
    NEGATIVE_IMPACT: 0.3,
    DEFAULT_VALUE: False,
    LOG: False
}


class BooleanIndicatorResponseCorrector(IndicatorResponseCorrector):
    """Class correcting the responses of indicators (of type bool)"""

    def __init__(self, context, indicator_id, params=DETECT_POSITIVE):
        """Instantiate with parameters
        
        :param context: The game context
        :param indicator_id: The id identifying the indicator
        :param params: Parameters passed to the corrector
        """
        super().__init__(context, indicator_id, params)

        self._value = self.params[DEFAULT_VALUE]

        if self.params[LOG]:
            logger = logging.getLogger("correctors")
            logger.info(
                "Type: New, Class: BooleanIndicatorResponseCorrector, Threshold: {}, Id: {}, Indicator: {}".format(
                    self.params[THRESHOLD], self.uuid, indicator_id))

    def update_value(self, indicator_response):
        """Update the current value according to positive and negative impacts
        
        :param indicator_response: Value of the response
        :type indicator_response: bool
        """

        if indicator_response:
            self._value += self.params[POSITIVE_IMPACT]
        else:
            self._value -= self.params[NEGATIVE_IMPACT]

        self._value = max(0, self._value)
        self._value = min(1, self._value)

        if self.params[LOG]:
            logger = logging.getLogger("correctors")
            update_time = self.context.cap.get(cv2.CAP_PROP_POS_MSEC)
            logger.info(
                "Time: {}, Type: Update, Class: BooleanIndicatorResponseCorrector, Id: {}, Indicator: {}, Value: {}, "
                "Correction: {}".format(
                    update_time,
                    self.uuid, self.indicator_id, int(indicator_response), round(self._value, 2)))

    @property
    def value(self):
        """Return value according to the corrector's threshold
        
        :return: A boolean based on the threshold
        :rtype: bool
        """

        return self._value >= self.params[THRESHOLD]
