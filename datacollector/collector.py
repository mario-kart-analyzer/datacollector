import argparse
import logging.config
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # avoid TensorFlow debugging messages

import sys

from datacollector.Context import Context
from datacollector.definitions import LOGGER_CONFIG_FILE, LOGS_FILE, LOGS_DIR, RACES_DIR


class ArgParser(object):
    """Argument parser class"""

    args = None
    parser = argparse.ArgumentParser()

    def __init__(self):
        """Instantiate parser"""

        self.parser.add_argument("-v", help="Post processing a video", default="", type=str)
        self.parser.add_argument("--debug_indicators", help="Open the indicator debug GUI", default=False, type=bool)
        self.parser.add_argument("--detect", help="Items or positions detection", default="positions", type=str)

    def parse_arguments(self):
        """Parse arguments"""

        self.args = self.parser.parse_args()

if __name__ == '__main__':
    arg_parser = ArgParser()
    arg_parser.parse_arguments()

    detections = ["positions", "items"]

    if arg_parser.args.detect not in detections:
        print("Detection unknown.")
        sys.exit()

    # create logs dir/file if they don't exist
    if not os.path.isdir(LOGS_DIR):
        os.mkdir(LOGS_DIR)

    if not os.path.isdir(RACES_DIR):
        os.mkdir(RACES_DIR)

    if not os.path.isfile(LOGS_FILE):
        open(LOGS_FILE, 'a').close()

    logging.config.fileConfig(LOGGER_CONFIG_FILE)
    mk_logger = logging.getLogger("mario_kart")

    if not arg_parser.args.v == "":
        # post-processing a video
        context = Context(video_name=arg_parser.args.v,
                          debug_indicators=arg_parser.args.debug_indicators,
                          detection=arg_parser.args.detect)
    else:
        # capturing from Elgato capture card
        context = Context(debug_indicators=arg_parser.args.debug_indicators,
                          detection=arg_parser.args.detect)
