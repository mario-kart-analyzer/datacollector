import os
import re
import time

import matplotlib.pyplot as plt

from datacollector.definitions import PROJECT_ROOT_DIR

correctors = {}


def get_operation_type(full_line):
    m = re.match(r".* Type: (.+?),", full_line)

    if m is None:
        return None

    return m.group(1)


def get_uuid(full_line):
    m = re.match(r".* Id: ([0-9a-f-]+?),", full_line)

    if m is None:
        return None

    return m.group(1)


def get_indicator_id(full_line):
    m = re.match(r".* Indicator: ([A-Za-z_]+)", full_line)

    if m is None:
        return None

    return m.group(1)


def get_indicator_class(full_line):
    m = re.match(r".* Class: (.+?),", full_line)

    if m is None:
        return None

    return m.group(1)


def get_time(full_line):
    m = re.match(r".* Time: (([0-9]*[.])?[0-9]+),", full_line)

    if m is None:
        return None

    return float(m.group(1))


def get_value(full_line):
    m = re.match(r".* Value: (.+?),", full_line)

    if m is None:
        return None

    return float(m.group(1))


def get_corrected_value(full_line):
    m = re.match(r".* Correction: (([0-9]*[.])?[0-9]+)", full_line)

    if m is None:
        return None

    return float(m.group(1))


def get_threshold(full_line):
    m = re.match(r".* Threshold: (([0-9]*[.])?[0-9]+)", full_line)

    if m is None:
        return None

    return float(m.group(1))


def new_corrector(full_line):
    uuid = get_uuid(full_line)

    fig = plt.figure()
    plt.ion()

    ax = plt.axes(xlim=(0, 10000), ylim=(-0.1, 1.1))

    indicator_class = get_indicator_class(full_line),
    indicator_id = get_indicator_id(full_line),
    fig.suptitle(indicator_id[0] + " - " + indicator_class[0])
    a0, = ax.plot([], [])
    a1, = ax.plot([], [])
    a2, = ax.plot([], [])

    correctors[uuid] = {
        "figure": plt.plot([], [])[0],
        "axes": ax,
        "value_plot": a0,
        "corrected_value_plot": a1,
        "threshold_plot": a2,
        "threshold": get_threshold(full_line),
        "time": [],
        "values": [],
        "corrected_values": []
    }


def update_corrector(full_line):
    uuid = get_uuid(full_line)
    update_time = get_time(full_line)
    value = get_value(full_line)
    corrected_value = get_corrected_value(full_line)

    if uuid not in correctors:
        return

    corrector = correctors[uuid]

    if len(corrector["time"]) > 0:
        first_time = corrector["time"][0]
    else:
        first_time = update_time

    corrector["time"].append(update_time)
    corrector["values"].append(value)
    corrector["corrected_values"].append(corrected_value)

    axes = corrector["axes"]
    value_plot = corrector["value_plot"]
    corrected_value_plot = corrector["corrected_value_plot"]
    threshold_plot = corrector["threshold_plot"]

    axes.set_xlim(first_time, update_time)

    value_plot.set_data(corrector["time"], corrector["values"])
    corrected_value_plot.set_data(corrector["time"], corrector["corrected_values"])
    threshold_plot.set_data(corrector["time"], corrector["threshold"])

    plt.draw()


def process(full_line: str):
    operation_type = get_operation_type(full_line)  # "New" or "Updating"

    if operation_type == "New":
        new_corrector(full_line)

    if operation_type == "Update":
        update_corrector(full_line)

    plt.pause(0.0001)


if __name__ == '__main__':
    f = open(os.path.join(PROJECT_ROOT_DIR, "logs", "corrector.log"), 'a+')  # Open at the end

    while True:
        line = ''
        while len(line) == 0 or line[-1] != '\n':
            tail = f.readline()
            if tail == '':
                time.sleep(0.1)  # avoid busy waiting
                continue
            line += tail
        process(line)
