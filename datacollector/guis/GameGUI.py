import json
import tkinter as tk
from threading import Thread

from decimal import Decimal

from datacollector.definitions import GAME_JSON_FILE


class GameGUI(Thread):
    def __init__(self):
        super().__init__()
        self.font = "Trebuchet MS"
        self.information_labels = ["Position", "Score", "Objet", "Tour", "Temps (course)", "Temps (tour)", "Delta"]

    def run(self):
        self.root = tk.Tk()
        self.root.wm_title("Mario Kart résumé")

        self.state_label_str = tk.StringVar()
        self.race_name_str = tk.StringVar()

        self.race_name_label = tk.Label(
            self.root, textvariable=self.race_name_str, anchor='center', font=(self.font, 30))
        self.race_name_label.grid(row=0, column=0, columnspan=2)

        self.p1_label_frame = tk.LabelFrame(self.root, text="Joueur 1", padx=12, pady=12)
        self.p2_label_frame = tk.LabelFrame(self.root, text="Joueur 2", padx=12, pady=12)
        self.p3_label_frame = tk.LabelFrame(self.root, text="Joueur 3", padx=12, pady=12)
        self.p4_label_frame = tk.LabelFrame(self.root, text="Joueur 4", padx=12, pady=12)

        self.p1_label_frame.grid(row=1, column=0, sticky='nw')
        self.p2_label_frame.grid(row=2, column=0, sticky='sw')
        self.p3_label_frame.grid(row=1, column=1, sticky='ne')
        self.p4_label_frame.grid(row=2, column=1, sticky='se')

        self.label_frames = [self.p1_label_frame, self.p2_label_frame, self.p3_label_frame, self.p4_label_frame]
        self.default_player_labels = []

        for f in self.label_frames:
            self.default_player_labels.append(
                tk.Label(f, text="Ce joueur n'est pas en jeu", anchor='center', font=(self.font, 18)))

        for l in self.default_player_labels:
            l.pack()

        self.state_label = tk.Label(self.root, textvariable=self.state_label_str, anchor='center', font=(self.font, 15))
        self.state_label.grid(row=3, column=0, columnspan=2)

        self.update()
        self.root.mainloop()

    def update(self):
        self._update_data()

        self.state_label_str.set(self.state_name)
        self.race_name_str.set(self._get_race_name())
        self._update_players()

        self.root.after(1000, self.update)

    def _update_data(self):
        try:
            with open(GAME_JSON_FILE, 'r') as json_file:
                json_string = json.load(json_file)
                json_object = json.loads(json_string)
        except FileNotFoundError:
                json_object = {}

        self.state_name = json_object.get('current_state', '')
        self.players_count = json_object.get('players_count', 0)
        self.race_name = json_object.get('current_race', '')
        self.players = json_object.get('players', {})

    def _update_players(self):
        if self.players_count < 4:
            for p in range(4 - self.players_count):
                self.default_player_labels[self.players_count - p - 1].configure(text="Ce joueur n'est pas en jeu", anchor='center')

        if self.players_count > 0:
            for p in range(self.players_count):
                self.default_player_labels[p].configure(text=self._get_player_label(p), anchor='w', justify='left')

    def _get_race_name(self):
        return self.race_name if self.race_name != '' else 'Pas de course en cours'

    def _get_player_label(self, player_id):
        label_str = ""
        for info in self.information_labels:
            label_str += "%s : %s\n" % (info, self._get_info(info, player_id))
        return label_str

    def _get_info(self, info, player_id):
        player_id = str(player_id + 1)

        try:
            if info == 'Position':
                return self.players[player_id]['race_position']
            elif info == 'Score':
                return self.players[player_id]['score']
            elif info == 'Objet':
                return self.players[player_id]['current_item']
            elif info == 'Tour':
                return self.players[player_id]['current_lap']
            elif info == 'Temps (course)':
                return "%s s" % float(round(Decimal(self.players[player_id]['race_times']["total"]), 3))
            elif info == 'Temps (tour)':
                lap_str = "\n  "
                for lap in self.players[player_id]['race_times']["laps"]:
                    lap_str += "%s s\n  " % float(round(lap, 3))
                return lap_str[:-3] if lap_str != "" else ""
            elif info == 'Delta':
                return "%s s" % float(round(Decimal(self.players[player_id]['race_times']["delta"]), 3))
        except KeyError:
            return ''
