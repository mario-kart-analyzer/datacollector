from threading import Thread
from tkinter import Tk, Label, Frame, GROOVE, LabelFrame


class DebugGUI(Thread):
    """GUI debugger class"""

    def __init__(self):
        super().__init__()
        self.start()
        self.labels = {}
        self.window = None
        self.stateLabel = None
        self.listFrame = None

    def close(self):
        self.window.quit()

    def run(self):
        self.window = Tk()
        self.window.protocol("WM_DELETE_WINDOW", self.close)

        self.stateLabel = Label(self.window, text="State id")
        self.stateLabel.pack()

        self.listFrame = Frame(self.window, borderwidth=2, relief=GROOVE)
        self.window.mainloop()

    def update_indicators(self, state, indicators_responses):
        self.stateLabel.config(text="State id: " + state)

        for indicator_id, indicator_response in indicators_responses.items():
            if indicator_id in self.labels:
                self.labels[indicator_id][0]['text'] = indicator_response
            else:
                lf = LabelFrame(self.listFrame, text=indicator_id, padx=20, pady=20)
                lf.pack(fill="both", expand="yes")

                l = Label(lf, text=indicator_response)
                self.labels[indicator_id] = (l, lf)
                l.pack()

        to_remove = []
        for indicator_id, (label, labelframe) in self.labels.items():
            if indicator_id not in indicators_responses:
                label.destroy()
                labelframe.destroy()
                to_remove.append(indicator_id)

        for indicator_id in to_remove:
            del self.labels[indicator_id]
        self.listFrame.pack()
