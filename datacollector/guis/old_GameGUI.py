from threading import Thread
from tkinter import Tk, Label, StringVar, LEFT


class GameGUI(Thread):
    """Game display GUI (deprecated)"""

    def __init__(self):
        super().__init__()
        self.window = None
        self.race_in_progress_label = None
        self.race_in_progress_str = None
        self.players_count_label = None
        self.players_count_str = None
        self.players_label = None
        self.players_str = None
        self.started = False

    def run(self):
        self.window = Tk()
        self.window.title("Mario Kart")
        self.window.protocol("WM_DELETE_WINDOW", self.close)

        self.race_in_progress_str = StringVar(self.window)
        self.race_in_progress_label = Label(self.window, textvariable=self.race_in_progress_str, fg="red",
                                            font=("Arial", 28))
        self.race_in_progress_str.set("Race not in progress")
        self.race_in_progress_label.pack()

        self.players_str = StringVar(self.window)
        self.players_label = Label(self.window, textvariable=self.players_str, fg="black", font=("Arial", 24),
                                   justify=LEFT)
        self.players_str.set("")
        self.players_label.pack()

        self.players_count_str = StringVar(self.window)
        self.players_count_label = Label(self.window, textvariable=self.players_count_str, fg="black",
                                         font=("Arial", 24))
        self.players_count_str.set("No player yet")
        self.players_count_label.pack()

        self.window.mainloop()

    def close(self):
        self.window.quit()

    def update(self, game):
        if not self.started \
                or self.race_in_progress_str is None \
                or self.players_str is None \
                or self.players_count_str is None:
            return

        if game.race_in_progress:
            self.race_in_progress_str.set("Race in progress:\n%s" % game.current_race)
        else:
            self.race_in_progress_str.set("Race not in progress")

        if game.players_count == 0:
            self.players_count_str.set("No player yet")
        elif game.players_count == 1:
            self.players_count_str.set("%d player" % game.players_count)
        else:
            self.players_count_str.set("%d players" % game.players_count)

        if game.players_count > 0 and any(game.players):
            self.players_str.set("".join([str(game.players[p]) for p in range(1, game.players_count + 1)]))
        else:
            self.players_str.set("")
