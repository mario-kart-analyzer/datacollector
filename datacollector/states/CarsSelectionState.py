from datacollector.Game import Game
from datacollector.corrector.BooleanIndicatorResponseCorrector import BooleanIndicatorResponseCorrector, DETECT_POSITIVE
from datacollector.indicators.menus_indicators.CarsMenuIndicator import CarsMenuIndicator
from datacollector.indicators.menus_indicators.CharacterSelectionIndicator import CharacterSelectionIndicator
from datacollector.indicators.menus_indicators.DetailsMenuIndicator import DetailsMenuIndicator
from datacollector.states.CharacterSelectionState import CharacterSelectionState
from datacollector.states.State import State


class CarsSelectionState(State):
    """Car selection state class"""

    id = "CARS_SELECTION_STATE"

    def __init__(self, context, params):
        super().__init__(context, params)

        self.found_players = False

        if "game" in self.params:
            self.game = self.params["game"]
        else:
            self.game = Game()

        self.game.current_state = self.id

        self.details_menu_indicator_corrector = BooleanIndicatorResponseCorrector(
            context, DetailsMenuIndicator.id, DETECT_POSITIVE)

        self.character_selection_indicator_corrector = BooleanIndicatorResponseCorrector(
            context, CharacterSelectionIndicator.id, DETECT_POSITIVE)

    @property
    def requested_indicators(self):
        """The necessary indicators to determine if whether we are in this state
        
        :return: A list of tuples containing the ids of the indicators
        :rtype: list of tuple
        """

        return [
            (CarsMenuIndicator.id, {}),
            (DetailsMenuIndicator.id, {}),
            (CharacterSelectionIndicator.id, {})
        ]

    def on_indicators_ready(self, indicators_responses: dict):
        """When every asked indicator is done processing, check in which state we are
        
        :param indicators_responses: The responses of the indicators
        :type indicators_responses: dict of (str, ?)
        """

        self.details_menu_indicator_corrector.update_value(indicators_responses[DetailsMenuIndicator.id])
        self.character_selection_indicator_corrector.update_value(indicators_responses[CharacterSelectionIndicator.id])
        cars_menu_indicator_response = indicators_responses[CarsMenuIndicator.id]

        if cars_menu_indicator_response["in_menu"] and not self.found_players:
            self.game.init_players(cars_menu_indicator_response["n_players_detected"])
            self.found_players = True

        if self.details_menu_indicator_corrector.value:  # in details menu
            from datacollector.states.DetailsMenuState import DetailsMenuState
            self.context.set_next_state({"state_id": DetailsMenuState.id, "game": self.game})
        elif self.character_selection_indicator_corrector.value:  # in game mode menu
            self.context.set_next_state({"state_id": CharacterSelectionState.id, "game": self.game})
