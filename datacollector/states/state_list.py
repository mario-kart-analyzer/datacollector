from datacollector.states.CarsSelectionState import CarsSelectionState
from datacollector.states.CharacterSelectionState import CharacterSelectionState
from datacollector.states.CupMenuState import CupMenuState
from datacollector.states.DetailsMenuState import DetailsMenuState
from datacollector.states.EndingScoreboardState import EndingScoreboardState
from datacollector.states.GameModeMenuState import GameModeMenuState
from datacollector.states.InGameState import InGameState
from datacollector.states.LoadingState import LoadingState
from datacollector.states.UnknownState import UnknownState
from datacollector.states.WelcomeMenuState import WelcomeMenuState

state_list = {
    UnknownState.id: UnknownState,
    LoadingState.id: LoadingState,
    InGameState.id: InGameState,
    EndingScoreboardState.id: EndingScoreboardState,
    DetailsMenuState.id: DetailsMenuState,
    CupMenuState.id: CupMenuState,
    WelcomeMenuState.id: WelcomeMenuState,
    CharacterSelectionState.id: CharacterSelectionState,
    CarsSelectionState.id: CarsSelectionState,
    GameModeMenuState.id: GameModeMenuState
}
