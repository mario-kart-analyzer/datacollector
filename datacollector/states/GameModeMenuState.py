from datacollector.Game import Game
from datacollector.corrector.BooleanIndicatorResponseCorrector import BooleanIndicatorResponseCorrector, DETECT_POSITIVE
from datacollector.indicators.menus_indicators.CharacterSelectionIndicator import CharacterSelectionIndicator
from datacollector.indicators.menus_indicators.WelcomeMenuIndicator import WelcomeMenuIndicator
from datacollector.states.State import State


class GameModeMenuState(State):
    """Game mode menu state class"""

    id = "GAME_MODE_MENU_STATE"

    def __init__(self, context, params):
        super().__init__(context, params)

        if "game" in self.params:
            self.game = self.params["game"]
        else:
            self.game = Game()

        self.game.current_state = self.id

        self.welcome_menu_indicator_corrector = BooleanIndicatorResponseCorrector(
            context, WelcomeMenuIndicator.id, DETECT_POSITIVE)

        self.character_selection_indicator_corrector = BooleanIndicatorResponseCorrector(
            context, CharacterSelectionIndicator.id, DETECT_POSITIVE)

    @property
    def requested_indicators(self):
        """The necessary indicators to determine if whether we are in this state
        
        :return: A list of tuples containing the ids of the indicators
        :rtype: list of tuple
        """

        return [
            (CharacterSelectionIndicator.id, {}),
            (WelcomeMenuIndicator.id, {})
        ]

    def on_indicators_ready(self, indicators_responses: dict):
        """When every asked indicator is done processing, check in which state we are
        
        :param indicators_responses: The responses of the indicators
        :type indicators_responses: dict of (str, ?)
        """

        self.character_selection_indicator_corrector.update_value(
            indicators_responses[CharacterSelectionIndicator.id])

        self.welcome_menu_indicator_corrector.update_value(bool(indicators_responses[WelcomeMenuIndicator.id]))

        if self.character_selection_indicator_corrector.value:  # forwards
            from datacollector.states.CharacterSelectionState import CharacterSelectionState
            self.context.set_next_state({"state_id": CharacterSelectionState.id, "game": self.game})
        elif self.welcome_menu_indicator_corrector.value:  # backwards
            from datacollector.states.WelcomeMenuState import WelcomeMenuState
            self.context.set_next_state({"state_id": WelcomeMenuState.id, "game": self.game})
