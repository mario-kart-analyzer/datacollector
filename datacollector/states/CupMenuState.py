from datacollector.Game import Game
from datacollector.corrector.BooleanIndicatorResponseCorrector import BooleanIndicatorResponseCorrector, DETECT_POSITIVE
from datacollector.indicators.LoadingIndicator import LoadingIndicator
from datacollector.indicators.menus_indicators.DetailsMenuIndicator import DetailsMenuIndicator
from datacollector.states.State import State


class CupMenuState(State):
    """Cup menu selection state class"""

    id = "CUP_MENU_STATE"

    def __init__(self, context, params):
        super().__init__(context, params)

        if "game" in self.params:
            self.game = self.params["game"]
        else:
            self.game = Game()

        self.game.current_state = self.id

        self.details_menu_indicator_corrector = BooleanIndicatorResponseCorrector(
            context, DetailsMenuIndicator.id, DETECT_POSITIVE)

        self.loading_indicator_corrector = BooleanIndicatorResponseCorrector(
            context, LoadingIndicator.id, DETECT_POSITIVE)

    @property
    def requested_indicators(self):
        """The necessary indicators to determine if whether we are in this state
        
        :return: A list of tuples containing the ids of the indicators
        :rtype: list of tuple
        """

        return [
            (DetailsMenuIndicator.id, {}),
            (LoadingIndicator.id, {})
        ]

    def on_indicators_ready(self, indicators_responses: dict):
        """When every asked indicator is done processing, check in which state we are
        
        :param indicators_responses: The responses of the indicators
        :type indicators_responses: dict of (str, ?)
        """

        # corrected responses
        self.details_menu_indicator_corrector.update_value(indicators_responses[DetailsMenuIndicator.id])
        self.loading_indicator_corrector.update_value(indicators_responses[LoadingIndicator.id])

        if self.details_menu_indicator_corrector.value:  # In details menu
            self.context.set_next_state({"state_id": DetailsMenuIndicator.id})
        elif self.loading_indicator_corrector.value:  # In loading
            self.context.set_next_state({"state_id": LoadingIndicator.id})
