from datacollector.corrector.BooleanIndicatorResponseCorrector import BooleanIndicatorResponseCorrector, DETECT_POSITIVE
from datacollector.indicators.InGameIndicator import InGameIndicator
from datacollector.indicators.LoadingIndicator import LoadingIndicator
from datacollector.indicators.menus_indicators.CarsMenuIndicator import CarsMenuIndicator
from datacollector.indicators.menus_indicators.CharacterSelectionIndicator import CharacterSelectionIndicator
from datacollector.indicators.menus_indicators.DetailsMenuIndicator import DetailsMenuIndicator
from datacollector.indicators.menus_indicators.GameModeMenuIndicator import GameModeMenuIndicator
from datacollector.indicators.menus_indicators.WelcomeMenuIndicator import WelcomeMenuIndicator
from datacollector.states.CarsSelectionState import CarsSelectionState
from datacollector.states.CharacterSelectionState import CharacterSelectionState
from datacollector.states.DetailsMenuState import DetailsMenuState
from datacollector.states.GameModeMenuState import GameModeMenuState
from datacollector.states.LoadingState import LoadingState
from datacollector.states.State import State
from datacollector.states.WelcomeMenuState import WelcomeMenuState


class UnknownState(State):
    """Unknown state class"""

    id = "UNKNOWN_STATE"

    def __init__(self, context, params):
        super().__init__(context, params)

        self.welcome_menu_indicator_corrector = BooleanIndicatorResponseCorrector(
            context, WelcomeMenuIndicator.id, DETECT_POSITIVE)

        self.game_mode_menu_indicator_corrector = BooleanIndicatorResponseCorrector(
            context, GameModeMenuIndicator.id, DETECT_POSITIVE)

        self.character_selection_indicator_corrector = BooleanIndicatorResponseCorrector(
            context, CharacterSelectionIndicator.id, DETECT_POSITIVE)

        self.cars_menu_indicator_corrector = BooleanIndicatorResponseCorrector(
            context, CarsMenuIndicator.id, DETECT_POSITIVE)

        self.details_menu_indicator_corrector = BooleanIndicatorResponseCorrector(
            context, DetailsMenuIndicator.id, DETECT_POSITIVE)

        self.loading_indicator_corrector = BooleanIndicatorResponseCorrector(
            context, LoadingIndicator.id, DETECT_POSITIVE)

    @property
    def requested_indicators(self):
        """The necessary indicators to determine if whether we are in this state

        :return: A list of tuples containing the ids of the indicators
        :rtype: list of tuple
        """

        return [
            (WelcomeMenuIndicator.id, {}),
            (GameModeMenuIndicator.id, {}),
            (CharacterSelectionIndicator.id, {}),
            (CarsMenuIndicator.id, {}),
            (DetailsMenuIndicator.id, {}),
            (LoadingIndicator.id, {}),
            (InGameIndicator.id, {})
        ]

    def on_indicators_ready(self, indicators_responses: dict):
        """When every asked indicator is done processing, check in which state we are

        :param indicators_responses: The responses of the indicators
        :type indicators_responses: dict of (str, ?)
        """

        # in_game_indicator_response = indicators_responses[InGameIndicator.id]  # to be implemented

        self.welcome_menu_indicator_corrector.update_value(indicators_responses[WelcomeMenuIndicator.id])
        self.game_mode_menu_indicator_corrector.update_value(indicators_responses[GameModeMenuIndicator.id])
        self.character_selection_indicator_corrector.update_value(indicators_responses[CharacterSelectionIndicator.id])
        self.cars_menu_indicator_corrector.update_value(indicators_responses[CarsMenuIndicator.id]["in_menu"])
        self.details_menu_indicator_corrector.update_value(indicators_responses[DetailsMenuIndicator.id])
        self.loading_indicator_corrector.update_value(indicators_responses[LoadingIndicator.id])

        if self.welcome_menu_indicator_corrector.value:  # in welcome menu
            self.context.set_next_state({"state_id": WelcomeMenuState.id})
        elif self.game_mode_menu_indicator_corrector.value:  # in game mode menu
            self.context.set_next_state({"state_id": GameModeMenuState.id})
        elif self.character_selection_indicator_corrector.value:  # in character selection
            self.context.set_next_state({"state_id": CharacterSelectionState.id})
        elif self.cars_menu_indicator_corrector.value:  # in cars menu
            self.context.set_next_state({"state_id": CarsSelectionState.id})
        elif self.details_menu_indicator_corrector.value:  # in details menu
            self.context.set_next_state({"state_id": DetailsMenuState.id})
        elif self.loading_indicator_corrector.value:  # in loading
            self.context.set_next_state({"state_id": LoadingState.id})
        # elif in_game_indicator_response:
        #     self.context.set_next_state({"state_id": InGameState.id})
