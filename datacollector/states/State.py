import abc


class State(object):
    id = None

    def __init__(self, context, params):
        self.context = context
        self.params = params
        pass

    @abc.abstractproperty
    def requested_indicators(self):
        """The necessary indicators to determine if whether we are in this state

        :return: A list of tuples containing the ids of the indicators
        :rtype: list of tuple
        """

        raise RuntimeWarning("requested_indicators property should not be called on parent")

    @abc.abstractmethod
    def on_indicators_ready(self, indicators_responses):
        """When every asked indicator is done processing, check in which state we are

        :param indicators_responses: The responses of the indicators
        :type indicators_responses: dict of (str, ?)
        """

        raise RuntimeWarning("on_indicators_ready property should not be called on parent")
