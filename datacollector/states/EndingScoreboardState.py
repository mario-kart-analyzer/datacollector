from datacollector.Game import Game
from datacollector.corrector.BooleanIndicatorResponseCorrector import BooleanIndicatorResponseCorrector, DETECT_POSITIVE
from datacollector.indicators.menus_indicators.WelcomeMenuIndicator import WelcomeMenuIndicator
from datacollector.states.State import State
from datacollector.states.WelcomeMenuState import WelcomeMenuState


class EndingScoreboardState(State):
    """Ending scoreboard state class"""

    id = "ENDING_SCOREBOARD_STATE"

    def __init__(self, context, params):
        super().__init__(context, params)

        if "game" in self.params:
            self.game = self.params["game"]
        else:
            self.game = Game()

        self.game.current_state = self.id

        self.welcome_menu_indicator_corrector = BooleanIndicatorResponseCorrector(
            context, WelcomeMenuIndicator.id, DETECT_POSITIVE)

    @property
    def requested_indicators(self):
        """The necessary indicators to determine if whether we are in this state
        
        :return: A list of tuples containing the ids of the indicators
        :rtype: list of tuple
        """


        return [
            (WelcomeMenuIndicator.id, {})
        ]

    def on_indicators_ready(self, indicators_responses: dict):
        """When every asked indicator is done processing, check in which state we are
        
        :param indicators_responses: The responses of the indicators
        :type indicators_responses: dict of (str, ?)
        """

        self.welcome_menu_indicator_corrector.update_value(indicators_responses[WelcomeMenuIndicator.id])

        if self.welcome_menu_indicator_corrector.value:
            self.context.set_next_state({"state_id": WelcomeMenuState.id, "game": self.game})
