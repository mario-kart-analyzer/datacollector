import logging
import threading
import time
from queue import Queue

import tensorflow as tf

import datacollector.learning.inception_v3 as inception
from datacollector.Game import Game
from datacollector.corrector.BooleanIndicatorResponseCorrector import BooleanIndicatorResponseCorrector, \
    DETECT_POSITIVE, DETECT_QUICK_POSITIVE
from datacollector.corrector.IntegerIndicatorResponseCorrector import IntegerIndicatorResponseCorrector, DETECT_POSITION
from datacollector.corrector.StringIndicatorResponseCorrector import StringIndicatorResponseCorrector
from datacollector.cutting.indicators.race_ending_cutter import crop_roi
from datacollector.definitions import RANK, RANKS_SCORES, LAP, ITEM
from datacollector.indicators.ItemIndicator import ItemIndicator
from datacollector.indicators.LapIndicator import LapIndicator
from datacollector.indicators.LoadingIndicator import LoadingIndicator
from datacollector.indicators.PositionIndicator import PositionIndicator
from datacollector.indicators.RaceBeginningIndicator import RaceBeginningIndicator
from datacollector.indicators.RaceDetectIndicator import RaceDetectIndicator
from datacollector.indicators.RaceEndingIndicator import RaceEndingIndicator
from datacollector.indicators.ScoreboardIndicator import ScoreboardIndicator
from datacollector.indicators.menus_indicators.EndingScoreboardIndicator import EndingScoreboardIndicator
from datacollector.states.State import State


class InGameState(State):
    """In game state class"""

    id = "IN_GAME_STATE"

    def __init__(self, context, params):
        super().__init__(context, params)
        self.mk_logger = logging.getLogger("mario_kart")
        self.context = context
        self.loading_menu_indicator_corrector = BooleanIndicatorResponseCorrector(
            self.context, LoadingIndicator.id, DETECT_POSITIVE)

        self.ending_scoreboard_indicator_corrector = BooleanIndicatorResponseCorrector(
            self.context, EndingScoreboardIndicator.id, DETECT_POSITIVE)

        if "game" in self.params:
            self.game = self.params["game"]
        else:
            self.game = Game(3)  # TODO : detect the number of players in game

        self.game.current_state = self.id
        self.game.race_in_progress = False
        self.race_began = False
        self.session = tf.Session()
        self.model = inception.Inception()

        for player_id, player in self.game.players.items():
            self.game.players[player_id].finished_race = False

        self.detections_queues = {
            "name": Queue(),
            "end": Queue(),
            "start": Queue(),
            "lap": Queue(),
            "pos": Queue(),
            "item": Queue(),
            "scoreboard": Queue()
        }

        self.detections_started = {
            "name": False,
            "end": False,
            "start": False,
            "lap": False,
            "pos": False,
            "item": False,
            "scoreboard": False
        }

    @property
    def requested_indicators(self):
        """The necessary indicators to determine if whether we are in this state

        :return: A list of tuples containing the ids of the indicators
        :rtype: list of tuple
        """

        indicators = [
            (LoadingIndicator.id, {})  # as long as we are not loading, we are in game
        ]

        if not self.game.race_in_progress and self.race_began:
            indicators.append((EndingScoreboardIndicator.id, {}))  # if the race has ended, watch out for the end

            if not self.detections_started["scoreboard"]:
                self.detections_started["scoreboard"] = True
                si = ScoreboardIndicator(
                    {"player_count": self.game.players_count, "scoreboard": self.game.last_scoreboard})
                ScoreboardDetector(self.context, self.game, si, self.detections_queues["scoreboard"]).start()

        if not self.detections_started["name"]:
            # looking for race's name in a new thread
            self.detections_started["name"] = True
            rdi = RaceDetectIndicator({})
            RaceNameDetector(self.context, rdi, self.detections_queues["name"]).start()

        elif not self.detections_started["start"]:
            # looking for race's start in a new thread
            self.detections_started["start"] = True
            rbi = RaceBeginningIndicator({})
            RaceBeginningDetector(self.context, self.game, rbi, self.detections_queues["start"]).start()

        elif self.game.race_in_progress and not self.detections_started["end"]:
            # looking for race's end in a new thread
            self.detections_started["end"] = True
            rei = RaceEndingIndicator({"player_count": self.game.players_count})
            RaceEndingDetector(self.context, self.game, rei, self.detections_queues["end"]).start()

            # starting lap detection at the same time
            self.detections_started["lap"] = True
            li = LapIndicator({"player_count": self.game.players_count})
            LapDetector(self.context, self.game, li, self.detections_queues["lap"]).start()

        if self.context.detection == "positions":  # for presentation
            if self.game.race_in_progress and not self.detections_started["pos"] and self.detections_started["end"]:
                # detecting the positions in another thread
                self.detections_started["pos"] = True
                pi = PositionIndicator({"player_count": self.game.players_count}, self.session, self.model)
                PositionDetector(self.context, self.game, pi, self.detections_queues["pos"]).start()

        elif self.context.detection == "items":  # for presentation
            if self.game.race_in_progress and not self.detections_started["item"] and self.detections_started["end"]:
                # detecting the items in another thread
                self.detections_started["item"] = True
                ii = ItemIndicator({"player_count": self.game.players_count}, self.session, self.model)
                ItemDetector(self.context, self.game, ii, self.detections_queues["item"]).start()

        return indicators

    def on_indicators_ready(self, indicators_responses: dict):
        """When every asked indicator is done processing, check in which state we are

        :param indicators_responses: The responses of the indicators
        :type indicators_responses: dict of (str, ?)
        """

        # corrected indicator responses
        self.loading_menu_indicator_corrector.update_value(indicators_responses[LoadingIndicator.id])

        if not self.game.race_in_progress and EndingScoreboardIndicator.id in indicators_responses:
            self.ending_scoreboard_indicator_corrector.update_value(indicators_responses[EndingScoreboardIndicator.id])

        # in game stats responses from other threads
        if not self.detections_queues["name"].empty():
            self.game.current_race = self.detections_queues["name"].get()
            if self.game.current_race == "":
                self.mk_logger.warning("Didn't manage to detect the race's name")

        if not self.detections_queues["start"].empty():
            self.game.race_in_progress = self.detections_queues["start"].get()
            self.race_began = True
            self.game.start_timers()

        if not self.detections_queues["end"].empty():
            self.game.race_in_progress = not self.detections_queues["end"].get()

        if not self.detections_queues["pos"].empty():
            self.process_rankings(self.detections_queues["pos"].get())

        if not self.detections_queues["item"].empty():
            self.process_items(self.detections_queues["item"].get())

        if not self.detections_queues["scoreboard"].empty():
            self.process_scoreboard(self.detections_queues["scoreboard"].get())

        # transitions towards other states
        if self.loading_menu_indicator_corrector.value:  # in loading
            self.detections_started["name"] = False
            self.detections_started["end"] = False
            self.game.race_in_progress = False

            self.game.save_state()
            self.model.close()
            self.session.close()

            from datacollector.states.LoadingState import LoadingState
            self.context.set_next_state({"state_id": LoadingState.id, "game": self.game})

        if self.ending_scoreboard_indicator_corrector.value:  # end of the races
            self.detections_started["name"] = False
            self.detections_started["end"] = False
            self.game.race_in_progress = False

            self.game.save_state()
            self.model.close()
            self.session.close()

            from datacollector.states.EndingScoreboardState import EndingScoreboardState
            self.context.set_next_state({"state_id": EndingScoreboardState.id, "game": self.game})

    def process_rankings(self, ranks):
        for i, player in self.game.players.items():
            if ranks[player.player_id][RANK] is None:
                self.mk_logger.warning("Could not detect rank of player " + str(i))

            player.race_position = ranks[player.player_id][RANK]

        self.game.update()

    def process_items(self, items):
        for i, player in self.game.players.items():
            if items[player.player_id][ITEM] is None:
                self.mk_logger.warning("Could not detect item of player " + str(i))

            player.change_item(items[player.player_id][ITEM])

        self.game.update()

    def process_scoreboard(self, scoreboard):
        self.game.last_scoreboard = scoreboard
        for pos, player in scoreboard.items():
            if player["type"] != "ai":
                p_id = int(player["type"][-1])
                self.game.players[p_id].score = player["score"]
                self.game.players[p_id].race_position = pos

        self.game.update()


class RaceNameDetector(threading.Thread):
    """Thread class used to detect the race name."""

    def __init__(self, context, rdi, queue):
        super().__init__()
        self.context = context
        self.rdi = rdi
        self.queue = queue

    def run(self):
        self.rdi.clear_images()

        for i in range(1, 21):
            self.rdi.append_image(self.context.frame)
            time.sleep(0.1)

        self.queue.put(self.rdi.detect_images())


class ScoreboardDetector(threading.Thread):
    """Thread class used to read at each race end the scoreboard"""

    def __init__(self, context, game, si, queue):
        super().__init__()
        self.context = context
        self.si = si
        self.game = game
        self.queue = queue

    def run(self):
        time.sleep(3.5)
        scoreboard = self.si.get_information(self.context.frame)
        self.queue.put(scoreboard)


class PositionDetector(threading.Thread):
    """Thread class used to detect the current position of each player."""

    def __init__(self, context, game, pi, queue):
        threading.Thread.__init__(self)
        self.context = context
        self.pi = pi
        self.game = game
        self.queue = queue
        self.correctors = {key: IntegerIndicatorResponseCorrector(self.context, PositionIndicator.id, DETECT_POSITION)
                           for key in range(1, self.game.players_count + 1)}

    def run(self):
        while self.game.race_in_progress:
            stats = self.pi.get_information(self.context.frame)
            for p in range(1, self.game.players_count + 1):
                self.correctors[p].update_value(stats[p][RANK])
                stats[p][RANK] = self.correctors[p].value

            self.queue.put(stats)
            time.sleep(0.33)


class ItemDetector(threading.Thread):
    """Thread class used to detect the current item of each player."""

    def __init__(self, context, game, ii, queue):
        super().__init__()
        self.context = context
        self.ii = ii
        self.game = game
        self.queue = queue
        self.correctors = {key: StringIndicatorResponseCorrector(self.context, ItemIndicator.id)
                           for key in range(1, self.game.players_count + 1)}

    def run(self):
        while self.game.race_in_progress:
            stats = self.ii.get_information(self.context.frame)
            for p in range(1, self.game.players_count + 1):
                self.correctors[p].update_value(stats[p][ITEM])
                stats[p][ITEM] = self.correctors[p].value

            self.queue.put(stats)
            time.sleep(0.25)


class RaceBeginningDetector(threading.Thread):
    """Thread class used to detect the race's beginning."""

    def __init__(self, context, game, rbi, queue):
        threading.Thread.__init__(self)
        self.context = context
        self.rbi = rbi
        self.game = game
        self.corrector = BooleanIndicatorResponseCorrector(
            self.context, RaceBeginningIndicator.id, DETECT_QUICK_POSITIVE)
        self.queue = queue

    def run(self):
        race_began = False

        while not race_began:
            time.sleep(0.1)
            self.corrector.update_value(self.rbi.get_information(self.context.frame))
            race_began = self.corrector.value

        self.queue.put(race_began)


class RaceEndingDetector(threading.Thread):
    """Thread class used to detect when the race ends."""

    def __init__(self, context, game, rei, queue):
        threading.Thread.__init__(self)
        self.context = context
        self.game = game
        self.rei = rei
        self.queue = queue
        self.correctors = {key: BooleanIndicatorResponseCorrector(self.context, RaceEndingIndicator.id, DETECT_POSITIVE)
                           for key in range(1, self.game.players_count + 1)}

    def run(self):
        ended = []
        race_ended = False
        previous_end_timer = 0

        for i in range(0, self.game.players_count):
            ended.append(False)

        while not race_ended:
            # check every player if they finished the race
            time.sleep(0.25)
            roi = crop_roi(self.context.frame, self.game.players_count)

            for i in range(0, self.game.players_count):
                self.correctors[i+1].update_value(self.rei.get_information(roi[i]))
                if self.correctors[i+1].value and not ended[i]:
                    ended[i] = True
                    self.game.players[i+1].finished_race = True
                    self.game.players[i+1].save_lap_timer()
                    self.game.players[i+1].stop_timer(previous_end_timer)
                    if previous_end_timer == 0:
                        previous_end_timer = self.game.players[i+1].race_times["total"]
                    self.game.update()

            race_ended = self.check_race_ended(ended)

        self.queue.put(ended)

    @staticmethod
    def check_race_ended(ended) -> bool:
        for e in ended:
            if not e:
                return False

        return True


class LapDetector(threading.Thread):
    """Thread class used to detect the current lap for each player."""

    def __init__(self, context, game, li, queue):
        threading.Thread.__init__(self)
        self.context = context
        self.li = li
        self.game = game
        self.queue = queue
        self.correctors = {key: IntegerIndicatorResponseCorrector(context, LapIndicator.id)
                           for key in range(self.game.players_count)}

    def run(self):
        while self.game.race_in_progress:
            i = 1
            stats = self.li.get_information(self.context.frame)

            for k, v in self.correctors.items():
                self.correctors[k].update_value(stats[i][LAP])
                stats[i][LAP] = self.correctors[k].value

                if self.correctors[k].value != self.game.players[i].current_lap:
                    self.game.players[i].current_lap = self.correctors[k].value
                    if self.correctors[k].value != 1 and self.correctors[k].value is not None:
                        self.game.players[i].save_lap_timer()

                i += 1

            self.queue.put(stats)
            time.sleep(0.5)
