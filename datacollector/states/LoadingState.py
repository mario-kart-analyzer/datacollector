from datacollector.Game import Game
from datacollector.corrector.BooleanIndicatorResponseCorrector import DETECT_NEGATIVE, BooleanIndicatorResponseCorrector
from datacollector.indicators.LoadingIndicator import LoadingIndicator
from datacollector.states.InGameState import InGameState
from datacollector.states.State import State


class LoadingState(State):
    """Loading state class"""

    id = "LOADING_STATE"

    def __init__(self, context, params):
        super().__init__(context, params)

        if "game" in self.params:
            self.game = self.params["game"]
        else:
            self.game = Game()

        self.game.current_state = self.id

        self.loading_menu_indicator_corrector = BooleanIndicatorResponseCorrector(
            self.context, LoadingIndicator.id, DETECT_NEGATIVE)

    @property
    def requested_indicators(self):
        """The necessary indicators to determine if whether we are in this state
        
        :return: A list of tuples containing the ids of the indicators
        :rtype: list of tuple
        """

        return [
            (LoadingIndicator.id, {})
        ]

    def on_indicators_ready(self, indicators_responses: dict):
        """When every asked indicator is done processing, check in which state we are
        
        :param indicators_responses: The responses of the indicators
        :type indicators_responses: dict of (str, ?)
        """

        # corrected loading indicator response
        self.loading_menu_indicator_corrector.update_value(indicators_responses[LoadingIndicator.id])

        if not self.loading_menu_indicator_corrector.value:  # Not loading anymore
            self.context.set_next_state({"state_id": InGameState.id, "game": self.game})
