from datacollector.Game import Game
from datacollector.corrector.BooleanIndicatorResponseCorrector import *
from datacollector.indicators.menus_indicators.GameModeMenuIndicator import GameModeMenuIndicator
from datacollector.states.GameModeMenuState import GameModeMenuState
from datacollector.states.State import State


class WelcomeMenuState(State):
    """Welcome menu state class"""

    id = "WELCOME_MENU_STATE"

    def __init__(self, context, params):
        super().__init__(context, params)

        if "game" in self.params:
            self.game = self.params["game"]
        else:
            self.game = Game()

        self.game.current_state = self.id

        self.game_mode_menu_indicator_corrector = BooleanIndicatorResponseCorrector(
            context, GameModeMenuIndicator.id, DETECT_POSITIVE)

    @property
    def requested_indicators(self):
        """The necessary indicators to determine if whether we are in this state

        :return: A list of tuples containing the ids of the indicators
        :rtype: list of tuple
        """

        return [
            (GameModeMenuIndicator.id, {})
        ]

    def on_indicators_ready(self, indicators_responses: dict):
        """When every asked indicator is done processing, check in which state we are

        :param indicators_responses: The responses of the indicators
        :type indicators_responses: dict of (str, ?)
        """

        self.game_mode_menu_indicator_corrector.update_value(indicators_responses[GameModeMenuIndicator.id])

        if self.game_mode_menu_indicator_corrector.value:
            self.context.set_next_state({"state_id": GameModeMenuState.id, "game": self.game})
