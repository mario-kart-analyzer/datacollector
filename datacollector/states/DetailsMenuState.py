from datacollector.Game import Game
from datacollector.corrector.BooleanIndicatorResponseCorrector import BooleanIndicatorResponseCorrector, DETECT_POSITIVE
from datacollector.indicators.LoadingIndicator import LoadingIndicator
from datacollector.indicators.menus_indicators.CarsMenuIndicator import CarsMenuIndicator
from datacollector.states.CarsSelectionState import CarsSelectionState
from datacollector.states.LoadingState import LoadingState
from datacollector.states.State import State


class DetailsMenuState(State):
    """Details menu selection state class"""

    id = "DETAILS_MENU_STATE"

    def __init__(self, context, params):
        super().__init__(context, params)

        if "game" in self.params:
            self.game = self.params["game"]
        else:
            self.game = Game()

        self.game.current_state = self.id

        self.loading_indicator_corrector = BooleanIndicatorResponseCorrector(
            context, LoadingIndicator.id, DETECT_POSITIVE)

        self.cars_menu_indicator_corrector = BooleanIndicatorResponseCorrector(
            context, CarsMenuIndicator.id, DETECT_POSITIVE)

    @property
    def requested_indicators(self):
        """The necessary indicators to determine if whether we are in this state
        
        :return: A list of tuples containing the ids of the indicators
        :rtype: list of tuple
        """

        return [
            (CarsMenuIndicator.id, {}),
            (LoadingIndicator.id, {})
        ]

    def on_indicators_ready(self, indicators_responses: dict):
        """When every asked indicator is done processing, check in which state we are
        
        :param indicators_responses: The responses of the indicators
        :type indicators_responses: dict of (str, ?)
        """

        self.cars_menu_indicator_corrector.update_value(indicators_responses[CarsMenuIndicator.id]["in_menu"])
        self.loading_indicator_corrector.update_value(indicators_responses[LoadingIndicator.id])

        if self.loading_indicator_corrector.value:  # In loading
            self.context.set_next_state({"state_id": LoadingState.id, "game": self.game})
        elif self.cars_menu_indicator_corrector.value:  # backwards
            self.context.set_next_state({"state_id": CarsSelectionState.id, "game": self.game})
