import os

import numpy as np

"""This file contains constants used throughout the source code"""

# Project directories
PROJECT_ROOT_DIR = os.path.dirname(os.path.abspath(os.path.join(__file__, os.pardir)))

DATACOLLECTOR_DIR = os.path.join(PROJECT_ROOT_DIR, 'datacollector')

LOGGER_CONFIG_FILE = os.path.join(PROJECT_ROOT_DIR, "mario_kart_logger.conf")
LOGS_DIR = os.path.join(PROJECT_ROOT_DIR, "logs")
LOGS_FILE = os.path.join(LOGS_DIR, "mario_kart_logger.log")

RACES_DIR = os.path.join(PROJECT_ROOT_DIR, "races")

GAME_JSON_FILE = os.path.join(PROJECT_ROOT_DIR, "game.json")

BENCHMARK_DIR = os.path.join(PROJECT_ROOT_DIR, "benchmark")

DATA_DIR = os.path.join(PROJECT_ROOT_DIR, "data")

VIDEO_DIR = os.path.join(DATA_DIR, "videos")
IMAGE_DIR = os.path.join(DATA_DIR, "images")

TEMPLATES_DIR = os.path.join(PROJECT_ROOT_DIR, "templates")

TEMPLATES_LOADING_DIR = os.path.join(TEMPLATES_DIR, "loading")
TEMPLATES_ENDING_SCOREBOARD_DIR = os.path.join(TEMPLATES_DIR, "ending_scoreboard")
TEMPLATES_DETAILS_MENU_DIR = os.path.join(TEMPLATES_DIR, "details_menu")
TEMPLATES_WELCOME_MENU_DIR = os.path.join(TEMPLATES_DIR, "welcome_menu")
TEMPLATES_CHARACTER_SELECTION_DIR = os.path.join(TEMPLATES_DIR, "character_selection")
TEMPLATES_CAR_MENU_DIR = os.path.join(TEMPLATES_DIR, "car_menu")
TEMPLATES_CUP_MENU_DIR = os.path.join(TEMPLATES_DIR, "cup_menu")
TEMPLATES_RACE_ENDING_DIR = os.path.join(TEMPLATES_DIR, "race_ending")
TEMPLATES_RANKS_IN_GAME_DIR = os.path.join(TEMPLATES_DIR, "ranks_in_game")
TEMPLATES_LAPS_IN_GAME_DIR = os.path.join(TEMPLATES_DIR, "laps")
TEMPLATES_GAME_MODE_MENU_DIR = os.path.join(TEMPLATES_DIR, "game_mode_menu")
TEMPLATES_RACE_BEGINNING_DIR = os.path.join(TEMPLATES_DIR, "race_beginning")

LEARNING_DIR = os.path.join(DATACOLLECTOR_DIR, "learning")
INCEPTION_MODEL_DIR = os.path.join(LEARNING_DIR, "model")

POSITIONS_MODEL_DIR = os.path.join(INCEPTION_MODEL_DIR, "positions")
POSITIONS_MODEL_CHECKPOINT = os.path.join(POSITIONS_MODEL_DIR, 'positions')
POSITIONS_MODEL_GRAPH = os.path.join(POSITIONS_MODEL_DIR, 'positions.meta')

ITEMS_MODEL_DIR = os.path.join(INCEPTION_MODEL_DIR, "items")
ITEMS_MODEL_CHECKPOINT = os.path.join(ITEMS_MODEL_DIR, "items")
ITEMS_MODEL_GRAPH = os.path.join(ITEMS_MODEL_DIR, "items.meta")

POSITIONS = [1, 10, 11, 12, 2, 3, 4, 5, 6, 7, 8, 9]

# Items' names list
ITEMS_NAMES = [
    "Banane",
    "Bill Balle",
    "Bloups",
    "Bob-omb",
    "Carapace bleue",
    "Carapace rouge",
    "Carapace verte",
    "Champignon turbo",
    "Champignon turbo doré",
    "Eclair",
    "Fleur boomerang",
    "Fleur de feu",
    "Grand 8",
    "Pièce",
    "Plante piranha",
    "Rien",
    "Super étoile",
    "Super klaxon",
    "Triple bananes",
    "Triple carapaces rouges",
    "Triple carapaces vertes",
    "Triple champignons"
]

# Stats categories
RANK = "rank"
COINS = "coins"
LAP = "lap"
ITEM = "item"

# Score bonus for each rank
RANKS_SCORES = {
    1: 15,
    2: 12,
    3: 10,
    4: 9,
    5: 8,
    6: 7,
    7: 6,
    8: 5,
    9: 4,
    10: 3,
    11: 2,
    12: 1
}

# Images' sizes
IMG_SIZE = (1920, 1080)
IMG_4_PLAYERS_SIZE = (IMG_SIZE[0]/2, IMG_SIZE[1]/2)
IMG_3_PLAYERS_SIZE = IMG_4_PLAYERS_SIZE
IMG_2_PLAYERS_SIZE = (IMG_SIZE[0]/2, IMG_SIZE[1])

# Stats-related images' sizes
IMG_RANK_SIZE = (100, 100)
IMG_ITEM_SIZE = (125, 125)
IMG_LAP_SIZE = (30, 30)

LAP_COORDS = (0.178, 0.201, 0.883, 0.935)

# Precomputed color means for each player
PLAYER_COLORS = {
    1: np.array((212.6, 88.3, 109.7, 0.0)),   # Yellow
    2: np.array((188.3, 144.1, 199.6, 0.0)),  # Blue
    3: np.array((150.7, 158.4, 75.0, 0.0)),   # Red
    4: np.array((224.0, 60.7, 173.05, 0.0)),  # Green
}

# Characters' names list
CHARACTERS_NAMES = [
    "Mario",
    "Luigi",
    "Peach",
    "Daisy",
    "Harmonie",
    "Mario de métal",
    "Yoshi",
    "Toad",
    "Koopa",
    "Maskass",
    "Lakitu",
    "Toadette",
    "Bébé Mario",
    "Bébé Luigi",
    "Bébé Peach",
    "Bébé Daisy",
    "Bébé Harmonie",
    "Peach d'or rose",
    "Bowser",
    "Donkey Kong",
    "Wario",
    "Waluigi",
    "Iggy",
    "Roy",
    "Lemmy",
    "Larry",
    "Wendy",
    "Ludwig",
    "Morton",
    "Mii",
    "Mario tanuki",
    "Peach chat",
    "Link",
    "Villageois(e)",
    "Marie",
    "Bowser Skelet"
]

# Races' name list
RACES_NAMES = [
    "Champidrome",
    "Parc Glougloop",
    "Piste aux délices",
    "Temple Thwomp",
    "Promenade Toad",
    "Manoir trempé",
    "Cascades Maskass",
    "Aéroport Azur",
    "Lagon Tourbillon",
    "Club Mario",
    "Descente givrée",
    "Voie céleste",
    "Désert Toussec",
    "Château de Bowser",
    "Route Arc-en-ciel",
    "Circuit Yoshi",
    "Arène d'Excitebike",
    "Route du dragon",
    "Mute City",
    "Parc Baby",
    "Pays Fromage",
    "Passage Feuillage",
    "Animal Crossing",
    "Prairie Meuh Meuh",
    "Circuit Mario",
    "Plage Cheep Cheep",
    "Autoroute Toad",
    "Désert Sec Sec",
    "Plaine Donut 3",
    "Autodrome royal",
    "Forêt tropicale DK",
    "Stade Wario",
    "Royaume Sorbet",
    "Piste musicale",
    "Vallée Yoshi",
    "Horloge Tic-Tac",
    "Égout Piranha",
    "Volcan grondant",
    "Mine Wario",
    "Station Glagla",
    "Circuit d'Hyrule",
    "Koopapolis",
    "Route Ruban",
    "Métro Turbo",
    "Big Blue"
]
