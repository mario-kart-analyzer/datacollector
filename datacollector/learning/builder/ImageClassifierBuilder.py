import os
import time
from datetime import timedelta

import matplotlib.cm as cm
import matplotlib.pyplot as plt
import numpy as np
import prettytensor as pt
import tensorflow as tf
from matplotlib.image import imread
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sklearn.metrics import confusion_matrix

import datacollector.learning.inception_v3 as inception
from datacollector.definitions import INCEPTION_MODEL_DIR
from datacollector.learning.dataset import load_cached


def load_images(image_paths):
    """Load the images from disk."""

    images = [imread(path) for path in image_paths]

    # Convert to a numpy array and return it.
    return np.asarray(images)


def classification_accuracy(correct):
    """Return the classification accuracy and the number of correct classifications."""

    # When averaging a boolean array, False means 0 and True means 1.
    # So we are calculating: number of True / len(correct) which is
    # the same as the classification accuracy.
    return correct.mean(), correct.sum()


class ImageClassifierBuilder(object):
    """This class allows caller to build an image classifier, by transfer learning, based on file structure:
    
    -> train_here
        -> class_1
            -> test
                test_image1
                test_image2
                ...
            train_image1
            train_image2
            ...
        -> class_2
        -> class_3
        ...
        
    It saves a checkpoint of the trained graph (in model/model_name/), which can be used later to make predictions.
    It uses the Inception v3 model.
    """

    def __init__(self, name, images_path):
        # Classifier name
        self.name = name

        # Training and test images folder path
        self.images_path = images_path

        # Classifier save path
        self.save_path = os.path.join(INCEPTION_MODEL_DIR, name)

        # Number of different image classes
        self.num_classes = 0

        # Names of image classes
        self.class_names = []

        # TensorFlow session instance
        self.session = None

        # Transfer values for train and test images set
        self.transfer_values_train = None
        self.transfer_values_test = None

        # Image paths and classes for the test images set
        self.image_paths_test = []
        self.cls_test = []

        # Labels for test and train images set
        self.labels_train = None
        self.labels_test = None

        # Training batch size
        self.train_batch_size = 64

        # TensorFlow variable which tracks the number of batches seen by the graph
        self.global_step = None

        # Adam optimizer used to improve the classifier graph
        self.optimizer = None

        # Accuracy of classifier based on test set
        self.accuracy = None

        # TensorFlow placeholder for images
        self.x = None

        # Real train images classes
        self.y_true = None

        # Predicted classes
        self.y_pred_cls = None

    def train(self, optimization_iterations=1000):
        """Train the image classifier for a certain number of optimization iterations (1000 by default)."""

        # Loading the training/testing data sets
        if not os.path.isdir(self.save_path):
            os.mkdir(self.save_path)

        print("Creating/loeading in cache DataSet object...")
        cache_name = os.path.join(self.save_path, str(self.name) + '_dataset.pkl')
        dataset = load_cached(cache_path=cache_name, in_dir=self.images_path)

        # Information regarding the data
        self.num_classes = dataset.num_classes
        self.class_names = dataset.class_names

        # Getting both training and testing sets
        image_paths_train, cls_train, self.labels_train = dataset.get_training_set()
        self.image_paths_test, self.cls_test, self.labels_test = dataset.get_test_set()

        print("Size of:")
        print(" - Training set:\t%s" % len(image_paths_train))
        print(" - Test set:\t%s" % len(self.image_paths_test))

        # Download the inception model and load it
        inception.maybe_download()
        model = inception.Inception()

        file_path_cache_train = os.path.join(self.save_path, 'inception-' + str(self.name) + '-train.pkl')
        file_path_cache_test = os.path.join(self.save_path, 'inception-' + str(self.name) + '-test.pkl')

        print("Processing Inception transfer values for training images...")

        # If transfer-values have already been calculated then reload them,
        # otherwise calculate them and save them to a cache-file.
        self.transfer_values_train = inception.transfer_values_cache(cache_path=file_path_cache_train,
                                                                     image_paths=image_paths_train,
                                                                     model=model)

        print("Processing Inception transfer values for test images...")

        # If transfer-values have already been calculated then reload them,
        # otherwise calculate them and save them to a cache-file.
        self.transfer_values_test = inception.transfer_values_cache(cache_path=file_path_cache_test,
                                                                    image_paths=self.image_paths_test,
                                                                    model=model)

        # self._plot_transfer_values_tsne(self.transfer_values_train, cls_train)

        print("Creating TensorFlow classifier...")

        # Make a new classifier in TensorFlow
        transfer_len = model.transfer_len

        self.x = tf.placeholder(tf.float32, shape=[None, transfer_len], name='x')
        self.y_true = tf.placeholder(tf.float32, shape=[None, self.num_classes], name='y_true')
        y_true_cls = tf.argmax(self.y_true, dimension=1)

        # Initialize the neural network using PrettyTensor
        x_pretty = pt.wrap(self.x)

        with pt.defaults_scope(activation_fn=tf.nn.relu):
            y_pred, loss = x_pretty. \
                fully_connected(size=1024, name='layer_fc1'). \
                softmax_classifier(num_classes=self.num_classes, labels=self.y_true)

        self.global_step = tf.Variable(initial_value=0, name='global_step', trainable=False)
        self.optimizer = tf.train.AdamOptimizer(learning_rate=1e-4).minimize(loss, self.global_step)

        self.y_pred_cls = tf.argmax(y_pred, dimension=1, name='y_pred_cls')
        correct_prediction = tf.equal(self.y_pred_cls, y_true_cls)
        self.accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

        # Create the TensorFlow session
        self.session = tf.Session()
        self.session.run(tf.global_variables_initializer())

        # Show differences between the first and the 1000th optimization iteration
        # and run the optimization
        self._print_test_accuracy(show_example_errors=False, show_confusion_matrix=True)
        self._optimize(num_iterations=optimization_iterations)
        self._print_test_accuracy(show_example_errors=False, show_confusion_matrix=True)

        # Save the model
        saved_vars = [k for k in tf.global_variables() if k.name.startswith(self.name)]
        print([n.name for n in tf.get_default_graph().as_graph_def().node])
        saver = tf.train.Saver(saved_vars)
        saver.save(self.session, os.path.join(self.save_path, str(self.name)))

        model.close()
        self.session.close()

    def _plot_scatter(self, values, cls):
        """Plot the transfer values using a color-map."""

        # Create a color-map with a different color for each class.
        cmap = cm.rainbow(np.linspace(0.0, 1.0, self.num_classes))

        # Create an index with a random permutation to make a better plot.
        idx = np.random.permutation(len(values))

        # Get the color for each sample.
        colors = cmap[cls[idx]]

        # Extract the x- and y-values.
        x = values[idx, 0]
        y = values[idx, 1]

        # Plot it.
        plt.scatter(x, y, color=colors, alpha=0.5)
        plt.show()

    def _print_test_accuracy(self, show_example_errors=False, show_confusion_matrix=False):
        """Print the testing set predictions accuracy."""

        # For all the images in the test-set,
        # calculate the predicted classes and whether they are correct.
        correct, cls_pred = self._predict_cls_test()

        # Classification accuracy and the number of correct classifications.
        acc, num_correct = classification_accuracy(correct)

        # Number of images being classified.
        num_images = len(correct)

        # Print the accuracy.
        msg = "Accuracy on test set: {0:.1%} ({1} / {2})"
        print(msg.format(acc, num_correct, num_images))

        # Plot some examples of mis-classifications, if desired.
        if show_example_errors:
            print("Example errors:")
            self._plot_example_errors(cls_pred=cls_pred, correct=correct)

        # Plot the confusion matrix, if desired.
        if show_confusion_matrix:
            print("Confusion Matrix:")
            self._plot_confusion_matrix(cls_pred=cls_pred)

    def _plot_confusion_matrix(self, cls_pred):
        """Print the confusion matrix."""

        # This is called from print_test_accuracy() below.

        # cls_pred is an array of the predicted class-number for
        # all images in the test-set.

        # Get the confusion matrix using sklearn.
        confusion = confusion_matrix(y_true=self.cls_test,  # True class for test-set.
                                     y_pred=cls_pred)  # Predicted class.

        # Print the confusion matrix as text.
        for i in range(self.num_classes):
            # Append the class-name to each line.
            class_name = "({}) {}".format(i, self.class_names[i])
            print(confusion[i, :], class_name)

        # Print the class-numbers for easy reference.
        class_numbers = [" ({0})".format(i) for i in range(self.num_classes)]
        print("".join(class_numbers))

    def _predict_cls(self, transfer_values, labels, cls_true):
        """Predict a class given transfer values."""

        # Number of images.
        num_images = len(transfer_values)

        # Split the data-set in batches of this size to limit RAM usage.
        batch_size = 256

        # Allocate an array for the predicted classes which
        # will be calculated in batches and filled into this array.
        cls_pred = np.zeros(shape=num_images, dtype=np.int)

        # Now calculate the predicted classes for the batches.
        # We will just iterate through all the batches.
        # There might be a more clever and Pythonic way of doing this.

        # The starting index for the next batch is denoted i.
        i = 0

        while i < num_images:
            # The ending index for the next batch is denoted j.
            j = min(i + batch_size, num_images)

            # Create a feed-dict with the images and labels
            # between index i and j.
            feed_dict = {self.x: transfer_values[i:j], self.y_true: labels[i:j]}

            # Calculate the predicted class using TensorFlow.
            cls_pred[i:j] = self.session.run(self.y_pred_cls, feed_dict=feed_dict)

            # Set the start-index for the next batch to the
            # end-index of the current batch.
            i = j

        # Create a boolean array whether each image is correctly classified.
        correct = (cls_true == cls_pred)

        return correct, cls_pred

    def _predict_cls_test(self):
        """Predict the classes of the testing set."""

        return self._predict_cls(transfer_values=self.transfer_values_test,
                                 labels=self.labels_test,
                                 cls_true=self.cls_test)

    def _random_batch(self):
        """Extract a random batch from the training set."""

        # Number of images (transfer-values) in the training-set.
        num_images = len(self.transfer_values_train)

        # Create a random index.
        idx = np.random.choice(num_images, size=self.train_batch_size, replace=False)

        # Use the random index to select random x and y-values.
        # We use the transfer-values instead of images as x-values.
        x_batch = self.transfer_values_train[idx]
        y_batch = self.labels_train[idx]

        return x_batch, y_batch

    def _optimize(self, num_iterations):
        """Optimize the model for a given number of iterations."""

        # Start-time used for printing time-usage below.
        start_time = time.time()

        for i in range(num_iterations):
            # Get a batch of training examples.
            # x_batch now holds a batch of images (transfer-values) and
            # y_true_batch are the true labels for those images.
            x_batch, y_true_batch = self._random_batch()

            # Put the batch into a dict with the proper names
            # for placeholder variables in the TensorFlow graph.
            feed_dict_train = {self.x: x_batch, self.y_true: y_true_batch}

            # Run the optimizer using this batch of training data.
            # TensorFlow assigns the variables in feed_dict_train
            # to the placeholder variables and then runs the optimizer.
            # We also want to retrieve the global_step counter.
            i_global, _ = self.session.run([self.global_step, self.optimizer], feed_dict=feed_dict_train)

            # Print status to screen every 100 iterations (and last).
            if (i_global % 100 == 0) or (i == num_iterations - 1):
                # Calculate the accuracy on the training-batch.
                batch_acc = self.session.run(self.accuracy, feed_dict=feed_dict_train)

                # Print status.
                msg = "Global Step: {0:>6}, Training Batch Accuracy: {1:>6.1%}"
                print(msg.format(i_global, batch_acc))

        # Ending time.
        end_time = time.time()

        # Difference between start and end-times.
        time_dif = end_time - start_time

        # Print the time-usage.
        print("Time usage: " + str(timedelta(seconds=int(round(time_dif)))))

    def _plot_example_errors(self, cls_pred, correct):
        """Plot wrong predictions examples."""

        # This function is called from print_test_accuracy() below.

        # cls_pred is an array of the predicted class-number for
        # all images in the test-set.

        # correct is a boolean array whether the predicted class
        # is equal to the true class for each image in the test-set.

        # Negate the boolean array.
        incorrect = np.invert(correct)

        # Get the indices for the incorrectly classified images.
        idx = incorrect.ravel().nonzero()[0]

        # Number of images to select, max 9.
        n = min(len(idx), 9)

        # Randomize and select n indices.
        idx = np.random.choice(idx,
                               size=n,
                               replace=False)

        # Get the predicted classes for those images.
        cls_pred = cls_pred[idx]

        # Get the true classes for those images.
        cls_true = self.cls_test[idx]

        # Load the corresponding images from the test-set.
        # Note: We cannot do image_paths_test[idx] on lists of strings.
        image_paths = [self.image_paths_test[i] for i in idx]
        images = load_images(image_paths)

        # Plot the images.
        self._plot_images(images=images, cls_true=cls_true, cls_pred=cls_pred)

    def _plot_images(self, images, cls_true, cls_pred=None, smooth=True):
        """Plot images and their prediction / correct classes."""

        assert len(images) == len(cls_true)

        # Create figure with sub-plots.
        fig, axes = plt.subplots(3, 3)

        # Adjust vertical spacing.
        if cls_pred is None:
            hspace = 0.3
        else:
            hspace = 0.6
        fig.subplots_adjust(hspace=hspace, wspace=0.3)

        # Interpolation type.
        if smooth:
            interpolation = 'spline16'
        else:
            interpolation = 'nearest'

        for i, ax in enumerate(axes.flat):
            # There may be less than 9 images, ensure it doesn't crash.
            if i < len(images):
                # Plot image.
                ax.imshow(images[i],
                          interpolation=interpolation)

                # Name of the true class.
                cls_true_name = self.class_names[cls_true[i]]

                # Show true and predicted classes.
                if cls_pred is None:
                    xlabel = "True: {0}".format(cls_true_name)
                else:
                    # Name of the predicted class.
                    cls_pred_name = self.class_names[cls_pred[i]]

                    xlabel = "True: {0}\nPred: {1}".format(cls_true_name, cls_pred_name)

                # Show the classes as the label on the x-axis.
                ax.set_xlabel(xlabel)

            # Remove ticks from the plot.
            ax.set_xticks([])
            ax.set_yticks([])

        # Ensure the plot is shown correctly with multiple plots
        plt.show()

    def _plot_transfer_values(self, i):
        """Plot the transfer values for a given test image."""

        print("Input image:")

        # Plot the i'th image from the test-set.
        image = imread(self.image_paths_test[i])
        plt.imshow(image, interpolation='spline16')
        plt.show()

        print("Transfer-values for the image using Inception model:")

        # Transform the transfer-values into an image.
        img = self.transfer_values_test[i]
        img = img.reshape((32, 64))

        # Plot the image for the transfer-values.
        plt.imshow(img, interpolation='nearest', cmap='Reds')
        plt.show()

    def _plot_transfer_values_tsne(self, transfer_values, cls):
        # Using PCA to reduce to 50 elements, then t-SNE (because t-SNE is slow so we reduce it first)

        pca = PCA(n_components=50)
        tsne = TSNE(n_components=2)

        transfer_values_50d = pca.fit_transform(transfer_values)
        transfer_values_reduced = tsne.fit_transform(transfer_values_50d)

        # Plot the transfer values reduced
        self._plot_scatter(transfer_values_reduced, cls=cls)
