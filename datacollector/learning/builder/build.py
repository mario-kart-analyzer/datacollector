import argparse

parser = argparse.ArgumentParser()
parser.add_argument("name", help="Classifier name", type=str)
parser.add_argument("path", help="Training and testing sets images path", type=str)

args = parser.parse_args()

import tensorflow as tf

from datacollector.learning.builder.ImageClassifierBuilder import ImageClassifierBuilder  # avoid useless modules load

with tf.variable_scope(args.name):
    builder = ImageClassifierBuilder(name=args.name, images_path=args.path)
    builder.train()
