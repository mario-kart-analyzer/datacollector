import logging.config
import os
from typing import Dict
from typing import Tuple

import cv2

from datacollector.definitions import VIDEO_DIR, IMG_SIZE
from datacollector.guis.DebugGUI import DebugGUI
from datacollector.indicators.indicator_list import indicator_list
from datacollector.states.UnknownState import UnknownState
from datacollector.states.state_list import state_list

mk_logger = logging.getLogger("mario_kart")


class Context(object):
    """Global game context"""

    def __init__(self, video_name="", debug_indicators=False, detection="positions"):
        """Instantiate the game context
        
        :param video_name: Video name
        :param debug_indicators: If true, display DebugGUI
        :param detection: Which information to detect in game (implemented for presentation)
        """

        self.state = None
        self.frame = None
        self.cap = None
        self.debug_indicators = debug_indicators
        self.detection = detection

        if not video_name == "":
            # process from a video file
            mk_logger.info("Began capturing from video: " + video_name)
            self.cap = cv2.VideoCapture(os.path.join(VIDEO_DIR, video_name))
        else:
            # record from the video flux
            self.cap = cv2.VideoCapture(1)  # not working yet
            mk_logger.info("Began capturing from the external capture card video flux")

        if debug_indicators:
            self.indicator_gui = DebugGUI()

        self.set_next_state({"state_id": UnknownState.id})
        self.loop()

    def loop(self):
        """The main loop method"""

        while True:
            ret, self.frame = self.cap.read()

            self.frame = cv2.resize(self.frame, IMG_SIZE)

            # Get current state requested indicators
            requested_indicators = self.state.requested_indicators

            indicators_responses = {}

            # Get the indicator(s) from their id(s)
            for requested_indicator_id, requested_indicators_params in requested_indicators:
                if requested_indicator_id not in indicator_list:
                    mk_logger.error(
                        "Requested id not found in indicator dictionary at index %s" % requested_indicator_id)
                    raise ValueError(
                        "Requested id not found in indicator dictionary at index %s" % requested_indicator_id)

                requested_indicator = indicator_list[requested_indicator_id](requested_indicators_params)

                # For each, get the result
                indicators_responses[requested_indicator_id] = requested_indicator.get_information(self.frame)

            # Notify the state that all indicators are ready
            self.state.on_indicators_ready(indicators_responses)

            if self.debug_indicators:
                self.indicator_gui.update_indicators(self.state.id, indicators_responses)
                cv2.imshow("Mario Kart", cv2.resize(self.frame, None, fx=0.5, fy=0.5, interpolation=cv2.INTER_CUBIC))
                k = cv2.waitKey(1)
                if k == 27:  # esc key
                    cv2.destroyAllWindows()
                    self.indicator_gui.close()
                    return

    def set_next_state(self, params):
        """Make the transition to the next state
        
        :param params: Parameters to pass from one state to another
        :type params: dict of (str, ?)
        """

        state_id = params["state_id"]

        if state_id not in state_list:
            mk_logger.error("State id not found in context dictionary at %s " % state_id)
            raise ValueError("State id not found in context dictionary at %s " % state_id)

        if self.state is not None:
            params["previous_state_id"] = self.state.id
        else:
            params["previous_state_id"] = None

        state_class = state_list[state_id]
        self.state = state_class(self, params)

        mk_logger.info("Transition from state %s to %s" % (params["previous_state_id"], state_id))
