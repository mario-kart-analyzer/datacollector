import os
from typing import List

import cv2
from numpy import ndarray


def images_in_folder(folder_path: str) -> List[ndarray]:
    images = [] # type: List[ndarray]
    for directory, subdirectories, files in os.walk(folder_path):
        for image_path in files:
            image_path = os.path.join(directory, image_path)  # type str
            images.append(cv2.imread(image_path))

    return images
