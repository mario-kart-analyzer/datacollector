from datacollector.indicators.menus_indicators.EndingScoreboardIndicator import EndingScoreboardIndicator
from tests.definitions import ENDING_SCOREBOARD_INDICATOR_SUCCESS_TEST_DATA_DIR, \
    ENDING_SCOREBOARD_INDICATOR_FAILURE_TEST_DATA_DIR
from tests.utils.images_in_folder import images_in_folder


def test_ending_scoreboard_indicator():
    ending_scoreboard_indicator = EndingScoreboardIndicator({})

    for image in images_in_folder(ENDING_SCOREBOARD_INDICATOR_SUCCESS_TEST_DATA_DIR):
        assert ending_scoreboard_indicator.get_information(image) is True

    for image in images_in_folder(ENDING_SCOREBOARD_INDICATOR_FAILURE_TEST_DATA_DIR):
        assert ending_scoreboard_indicator.get_information(image) is False
