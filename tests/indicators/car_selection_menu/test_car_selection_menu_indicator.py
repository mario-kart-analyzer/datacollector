import cv2

import tests.definitions
from datacollector.definitions import IMG_SIZE
from datacollector.indicators.menus_indicators.CarsMenuIndicator import CarsMenuIndicator
from tests.utils.images_in_folder import images_in_folder


def test_car_selection_menu_indicator():
    car_selection_menu_indicator = CarsMenuIndicator({})
    for image in images_in_folder(tests.definitions.CAR_SELECTOR_MENU_INDICATOR_SUCCESS_TEST_DATA_DIR):
        image = cv2.resize(image, IMG_SIZE)
        assert car_selection_menu_indicator.get_information(image)["in_menu"] is True
        assert car_selection_menu_indicator.get_information(image)["players_detected"] == {
                1: True,
                2: True,
                3: False,
                4: False,
            }

    for image in images_in_folder(tests.definitions.CAR_SELECTOR_MENU_INDICATOR_FAILURE_TEST_DATA_DIR):
        assert car_selection_menu_indicator.get_information(image)["in_menu"] is False
