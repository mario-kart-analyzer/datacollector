from datacollector.indicators.menus_indicators.DetailsMenuIndicator import DetailsMenuIndicator
from tests.definitions import DETAILS_MENU_INDICATOR_SUCCESS_TEST_DATA_DIR, DETAILS_MENU_INDICATOR_FAILURE_TEST_DATA_DIR
from tests.utils.images_in_folder import images_in_folder


def test_details_menu_indicator():
    """
    Test function for the details menu indicator
    """
    details_menu_indicator = DetailsMenuIndicator({})

    for image in images_in_folder(DETAILS_MENU_INDICATOR_SUCCESS_TEST_DATA_DIR):
        assert details_menu_indicator.get_information(image) is True

    for image in images_in_folder(DETAILS_MENU_INDICATOR_FAILURE_TEST_DATA_DIR):
        assert details_menu_indicator.get_information(image) is False
