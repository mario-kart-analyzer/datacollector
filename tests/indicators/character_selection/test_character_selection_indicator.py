from datacollector.indicators.menus_indicators.CharacterSelectionIndicator import CharacterSelectionIndicator
from tests.definitions import CHARACTER_SELECTION_INDICATOR_SUCCESS_TEST_DATA_DIR,\
    CHARACTER_SELECTION_INDICATOR_FAILURE_TEST_DATA_DIR
from tests.utils.images_in_folder import images_in_folder


def test_character_selection_indicator():
    character_selection_indicator = CharacterSelectionIndicator({})

    for image in images_in_folder(CHARACTER_SELECTION_INDICATOR_SUCCESS_TEST_DATA_DIR):
        assert character_selection_indicator.get_information(image) is True

    for image in images_in_folder(CHARACTER_SELECTION_INDICATOR_FAILURE_TEST_DATA_DIR):
        assert character_selection_indicator.get_information(image) is False
