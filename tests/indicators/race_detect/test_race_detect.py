from datacollector.indicators.RaceDetectIndicator import RaceDetectIndicator
from tests.definitions import RACE_DETECT_INDICATOR_SUCCESS_TEST_DATA_DIR, RACE_DETECT_INDICATOR_FAILURE_TEST_DATA_DIR
from tests.utils.images_in_folder import images_in_folder


def test_loading_indicator():
    race_detect_indicator = RaceDetectIndicator({})
    valid_races_name = ["Prairie Meuh Meuh", "Royaume Sorbet", "Route Arc-en-ciel", "Club Mario"]

    for image in images_in_folder(RACE_DETECT_INDICATOR_SUCCESS_TEST_DATA_DIR):
        assert (race_detect_indicator.get_information(image) in valid_races_name) is True

    for image in images_in_folder(RACE_DETECT_INDICATOR_FAILURE_TEST_DATA_DIR):
        assert race_detect_indicator.get_information(image) == ""
