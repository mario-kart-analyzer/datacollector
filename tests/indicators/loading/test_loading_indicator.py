from datacollector.indicators.LoadingIndicator import LoadingIndicator
from tests.definitions import LOADING_INDICATOR_SUCCESS_TEST_DATA_DIR, LOADING_INDICATOR_FAILURE_TEST_DATA_DIR
from tests.utils.images_in_folder import images_in_folder


def test_loading_indicator():
    loading_indicator = LoadingIndicator({})

    for image in images_in_folder(LOADING_INDICATOR_SUCCESS_TEST_DATA_DIR):
        assert loading_indicator.get_information(image) is True

    for image in images_in_folder(LOADING_INDICATOR_FAILURE_TEST_DATA_DIR):
        assert loading_indicator.get_information(image) is False
