from datacollector.indicators.menus_indicators.WelcomeMenuIndicator import WelcomeMenuIndicator
from tests.definitions import WELCOME_MENU_INDICATOR_SUCCESS_TEST_DATA_DIR, WELCOME_MENU_INDICATOR_FAILURE_TEST_DATA_DIR
from tests.utils.images_in_folder import images_in_folder


def test_welcome_menu_indicator():
    welcome_menu_indicator = WelcomeMenuIndicator({})

    for image in images_in_folder(WELCOME_MENU_INDICATOR_SUCCESS_TEST_DATA_DIR):
        assert welcome_menu_indicator.get_information(image) is True

    for image in images_in_folder(WELCOME_MENU_INDICATOR_FAILURE_TEST_DATA_DIR):
        assert welcome_menu_indicator.get_information(image) is False
