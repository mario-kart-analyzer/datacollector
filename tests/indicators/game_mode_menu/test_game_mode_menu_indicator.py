from datacollector.indicators.menus_indicators.GameModeMenuIndicator import GameModeMenuIndicator
from tests.definitions import GAME_MODE_MENU_INDICATOR_SUCCESS_TEST_DATA_DIR, \
    GAME_MODE_MENU_INDICATOR_FAILURE_TEST_DATA_DIR
from tests.utils.images_in_folder import images_in_folder


def test_game_mode_menu_indicator():
    """
    Test function for the game mode menu indicator
    """
    game_mode_menu_indicator = GameModeMenuIndicator({})

    for image in images_in_folder(GAME_MODE_MENU_INDICATOR_SUCCESS_TEST_DATA_DIR):
        assert game_mode_menu_indicator.get_information(image) is True

    for image in images_in_folder(GAME_MODE_MENU_INDICATOR_FAILURE_TEST_DATA_DIR):
        assert game_mode_menu_indicator.get_information(image) is False
