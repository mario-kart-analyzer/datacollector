from datacollector.indicators.LapIndicator import LapIndicator
from tests.definitions import LAP_INDICATOR_SUCCESS_2_TEST_DATA_DIR, LAP_INDICATOR_SUCCESS_3_TEST_DATA_DIR
from tests.utils.images_in_folder import images_in_folder


def test_lap_indicator():
    lap_indicator = LapIndicator({"player_count": 3})

    for image in images_in_folder(LAP_INDICATOR_SUCCESS_3_TEST_DATA_DIR):
        assert lap_indicator.get_information(image) == {1: {'lap': 1}, 2: {'lap': 1}, 3: {'lap': 1}}
