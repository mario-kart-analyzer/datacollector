import tests.definitions
from datacollector.indicators.menus_indicators.CupMenuIndicator import CupMenuIndicator
from tests.utils.images_in_folder import images_in_folder


def test_cup_selection_menu_indicator():
    cup_menu_indicator = CupMenuIndicator({})

    for image in images_in_folder(tests.definitions.CUP_MENU_INDICATOR_SUCCESS_TEST_DATA_DIR):
        assert cup_menu_indicator.get_information(image) is True

    for image in images_in_folder(tests.definitions.CUP_MENU_INDICATOR_FAILURE_TEST_DATA_DIR):
        assert cup_menu_indicator.get_information(image) is False
