from datacollector.indicators.RaceBeginningIndicator import RaceBeginningIndicator
from tests.definitions import RACE_BEGINNING_INDICATOR_SUCCESS_TEST_DATA_DIR, \
    RACE_BEGINNING_INDICATOR_FAILURE_TEST_DATA_DIR
from tests.utils.images_in_folder import images_in_folder


def test_race_beginning_indicator():
    race_beginning_indicator = RaceBeginningIndicator({})

    for image in images_in_folder(RACE_BEGINNING_INDICATOR_SUCCESS_TEST_DATA_DIR):
        assert race_beginning_indicator.get_information(image) is True

    for image in images_in_folder(RACE_BEGINNING_INDICATOR_FAILURE_TEST_DATA_DIR):
        assert race_beginning_indicator.get_information(image) is False
